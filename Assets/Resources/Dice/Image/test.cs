using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class test : MonoBehaviour
{
    public Sprite[] numberImage = new Sprite[9];
    public GameObject obj;
    public Image image;

    private DiceManager diceManager;
    private bool diceFlag = true;

    // Start is called before the first frame update
    void Start()
    {
        image.enabled = false;
        diceManager = obj.GetComponent<DiceManager>();
    }

    // Update is called once per frame
    void Update()
    {
        bool flag = image.enabled;

        if (Input.GetKeyUp(KeyCode.Space))
        {
            if (diceFlag)
            {
                image = image.GetComponent<Image>();
                image.sprite = numberImage[diceManager.GetDiceResult()];
                flag = true;
                diceFlag = false;
            }
            else
            {
                flag = false;
                diceFlag = true;
            }
        }

            image.enabled = flag;
    }
}
