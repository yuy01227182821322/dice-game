using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestImage : MonoBehaviour
{
    public Sprite[] numberImage = new Sprite[9];
    public GameObject obj;
    public Image image;

    private TestManager diceManager;
    private bool diceFlag = false;
    private int cnt = 700;

    // Start is called before the first frame update
    void Start()
    {
        image.enabled = false;
        diceManager = obj.GetComponent<TestManager>();
    }

    // Update is called once per frame
    void Update()
    {
        bool flag = image.enabled;

        if (Input.GetKeyUp(KeyCode.Space))
        {
            diceFlag = true;
        }

        if(diceFlag)
        {
            if(cnt < 500)
            {
                image = image.GetComponent<Image>();
                image.sprite = numberImage[diceManager.GetDiceRoll()];
                flag = true;
            }
            if(cnt < 0)
            {
                flag = false;
                diceFlag = false;
                cnt = 700;
            }
            cnt--;
        }

        image.enabled = flag;
    }
}
