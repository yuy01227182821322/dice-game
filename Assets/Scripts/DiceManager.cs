using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceManager : MonoBehaviour
{
    public GameObject[] dicePrefab = new GameObject[5];
    public GameObject smoke;
    public int diceType = 0;
    public int diceQuantity = 0;
    [SerializeField]
    private Transform canvas_;
    [SerializeField]
    private GameObject diceNumObj_;
    private DiceNumber diceNum_;

    private List<DiceRoll> dice_;
    private GameObject smokeObj;
    private Explosion expEffect_;
    private bool rollFlag_;
    private int diceResult;
    private bool isEnd_ = false;
    public bool IsEnd => isEnd_;

    // Start is called before the first frame update
    void Start()
    {
        Init();
        diceNum_ = diceNumObj_.GetComponent<DiceNumber>();
    }

    public void Init()
	{
        isEnd_ = false;
        diceResult = 0;
        rollFlag_ = false;
        dice_ = new List<DiceRoll>();
    }

    // Update is called once per frame
    void Update()
    {
        if(rollFlag_)
		{
            // 爆発エフェクト削除
            if(expEffect_.EndFlag)
			{
                Destroy(smokeObj);
			}
            // 数字表示終了時
            if(!diceNum_.gameObject.activeSelf)
			{
                isEnd_ = true;
            }
        }
    }
    public void CreateDice(int diceType = 0)
    {
        if(diceQuantity == 0)
        {
            // 生成位置
            Vector3 pos = new Vector3(0.0f, 0.0f, 0.0f);
            // プレハブを指定位置に生成
            var obj = Instantiate(dicePrefab[diceType], canvas_, false);
            obj.layer = canvas_.gameObject.layer;
            obj.transform.localScale *= 100.0f;
            dice_.Add(obj.GetComponent<DiceRoll>());
        }
        else if(diceQuantity == 1)
        {
            // 生成位置
            Vector3 pos_1 = new Vector3(-2.0f, 0.0f, 0.0f);
            // プレハブを指定位置に生成
            var obj_1 = Instantiate(dicePrefab[diceType], pos_1, Quaternion.identity, canvas_);
            dice_.Add(obj_1.GetComponent<DiceRoll>());

            // 生成位置
            Vector3 pos_2 = new Vector3(2.0f, 0.0f, 0.0f);
            // プレハブを指定位置に生成
            var obj_2 = Instantiate(dicePrefab[diceType], pos_2, Quaternion.identity, canvas_);
            dice_.Add(obj_2.GetComponent<DiceRoll>());
        }
        else
        {
            // 生成位置
            Vector3 pos_1 = new Vector3(3.5f, 0.0f, 0.0f);
            // プレハブを指定位置に生成
            var obj_1 = Instantiate(dicePrefab[diceType], pos_1, Quaternion.identity, canvas_);
            dice_.Add(obj_1.GetComponent<DiceRoll>());

            // 生成位置
            Vector3 pos_2 = new Vector3(0.0f, 0.0f, 0.0f);
            // プレハブを指定位置に生成
            var obj_2 = Instantiate(dicePrefab[diceType], pos_2, Quaternion.identity, canvas_);
            dice_.Add(obj_2.GetComponent<DiceRoll>());

            // 生成位置
            Vector3 pos_3 = new Vector3(-3.5f, 0.0f, 0.0f);
            // プレハブを指定位置に生成
            var obj_3 = Instantiate(dicePrefab[diceType], pos_3, Quaternion.identity, canvas_);
            dice_.Add(obj_3.GetComponent<DiceRoll>());
        }
    }

    public void DestroyDice()
	{
        foreach(var d in dice_)
		{
            Destroy(d.gameObject);
		}
	}


    public void Roll()
	{
        
        if(!rollFlag_)
        {
            foreach(var d in dice_)
            {
                // さいころを振る今は一つのみ
                d.Roll();
                diceResult = d.GetDiceRoll();
                Debug.Log(diceResult);
                diceNum_.Active();
                // 生成位置
                Vector3 pos = d.transform.position;
                // 生成キャンバスの下にUIとして生成
                smokeObj = Instantiate(smoke, canvas_, false);
                smokeObj.transform.position = d.transform.position - new Vector3(0, 0, -500);
                smokeObj.transform.localScale *= 100.0f;
                smokeObj.layer = canvas_.gameObject.layer;
                // プレハブを指定位置に生成
                expEffect_ = smokeObj.GetComponent<Explosion>();

                rollFlag_ = true;
            }
            DestroyDice();
        }
    }

    public int GetDiceResult()
    {
        return diceResult;
    }
}
