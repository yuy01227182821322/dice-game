using UnityEngine;

public class TitleScene : MonoBehaviour
{
    SceneController sceneController;
    // Start is called before the first frame update
    void Start()
    {
        sceneController = GameObject.Find("SceneController").GetComponent<SceneController>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnClickButton()
    {
        sceneController.NextScene();
    }
}
