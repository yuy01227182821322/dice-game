using System.Xml.Linq;
using System;
using System.Collections.Generic;
using UnityEngine;

public class XMLLoader
{


    // 最大ルート数
    private int maxRoot_;
    public int MaxRoot { get { return this.maxRoot_; } }


    public bool LoadXml(string path,ref List<RootState> rootStates)
    {
        XDocument doc = XDocument.Parse(path);
        if (doc == null)
        {
            Debug.LogError("XML読み込み失敗");
            return false;
        }
        Debug.Log("XML読み込み成功");
        XElement map = doc.Element("map");
        if (map.IsEmpty)
        {
            Debug.LogError("マップエレメント読み込み失敗");
            return false;
        }
        Debug.Log("マップエレメント読み込み成功");
        if(!LoadChip(in map,ref rootStates))
        {
            return false;
        }
        //doc.Save(path);
        return true;
    }

    //========================================================================================================
    // チップの読み込み
    private bool LoadChip(in XElement map,ref List<RootState> rootStates)
    {
        Debug.Log("チップの読み込み開始");

        var chips = map.Element("chip");
        if (chips == null)
        {
            Debug.LogError("チップエレメント読み込み失敗");
            return false;
        }
        Debug.Log("チップエレメント読み込み成功");
        maxRoot_ = int.Parse(chips.Attribute("max_root").Value);

        var roots = chips.Elements();
        if (chips == null)
        {
            Debug.LogError("ルートエレメント読み込み失敗");
            return false;
        }
        Debug.Log("ルートエレメント読み込み成功");

        Func<string, MasuType> TypeSet = s =>
        {
            MasuType type = MasuType.NULL;
            switch (s)
            {
                case "normal":
                    type = MasuType.NORMAL;
                    break;
                case "start":
                    type = MasuType.START;
                    break;
                case "end":
                    type = MasuType.END;
                    break;
                case "branch":
                    type = MasuType.BRANCH;
                    break;
                case "damage":
                    type = MasuType.DAMAGE;
                    break;
                case "recovery":
                    type = MasuType.RECOVERY;
                    break;
                case "branchMasu":
                    type = MasuType.BRANCH_MASU;
                    break;
                case "item":
                    type = MasuType.ITEM;
                    break;
                case "redice":
                    type = MasuType.REDICE;
                    break;
                case "frontMove":
                    type = MasuType.FRONT_MOVE;
                    break;
                case "backMove":
                    type = MasuType.BACK_MOVE;
                    break;
                case "itemLost":
                    type = MasuType.ITEM_LOST;
                    break;
                default:
                    type = MasuType.NULL;
                    break;
            }
            return type;
        };

        foreach (var xRoot in roots)
        {
            Debug.Log("ID:" + xRoot.Attribute("id").Value);
            RootState state = new RootState();
            state.id = int.Parse(xRoot.Attribute("id").Value);
            state.masuNum = int.Parse(xRoot.Attribute("masuNum").Value);
            state.connectBranch1 = int.Parse(xRoot.Attribute("connectBranch1").Value);
            state.connectBranch2 = int.Parse(xRoot.Attribute("connectBranch2").Value);
            state.masuList = new List<MasuState>();
            var xMasus = xRoot.Elements();
            if (roots == null)
            {
                continue;
            }
            foreach (var xMasu in xMasus)
            {
                MasuState masu = new MasuState();
                masu.id = int.Parse(xMasu.Attribute("id").Value);
                masu.pos = new Vector3(float.Parse(xMasu.Attribute("x").Value), float.Parse(xMasu.Attribute("y").Value), float.Parse(xMasu.Attribute("z").Value));
                masu.type = TypeSet(xMasu.Attribute("type").Value);
                masu.num = new List<string>();
                masu.num.Add(xMasu.Attribute("num1").Value);
                masu.num.Add(xMasu.Attribute("num2").Value);
                masu.num.Add(xMasu.Attribute("num3").Value);
                masu.num.Add(xMasu.Attribute("num4").Value);
                state.masuList.Add(masu);
            }
            rootStates.Add(state);
        }

        Debug.Log("チップの読み込み終了");
        return true;
    }
}
