using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceRoll : MonoBehaviour
{
	public enum diceList
    {
		Normal,
		Small,
		Big,
		Odd,
		Even
    };

	public diceList dicelist;

	private bool rotateFlag_ = true;
	private int roll;
	private int diceRoll;
	private int cnt = 200;

	private Vector3[] dice = new[] { new Vector3(0, 180, 0), new Vector3(0, 90, 90), new Vector3(90, 0, 90),
							 new Vector3(90, 0, -90), new Vector3(-90, 0, 0), new Vector3(0, 0, -90) };
	private Vector3[] rot = new[] { new Vector3(1, 0, 1), new Vector3(0, 1, 1), new Vector3(1, 1, 0),
							new Vector3(1, 0, -1), new Vector3(0, -1, 1), new Vector3(-1, 1, 0),
							new Vector3(-1, 0, 1), new Vector3(0, 1, -1), new Vector3(1, -1, 0)};


	// Update is called once per frame
	void Update()
	{
		if (rotateFlag_)
		{
			cnt--;
			if (cnt < 0)
			{
				roll = Random.Range(0, 6);
				cnt = 200;
			}
			transform.Rotate(rot[roll], Space.World);
		}
	}
	// さいころを振る
	public void Roll()
	{
		diceRoll = Random.Range(0, 6);
		this.transform.rotation = Quaternion.Euler(dice[diceRoll]);
		rotateFlag_ = false;
	}

	// 出た目を取得する
	public int GetDiceRoll()
	{
		if (dicelist == diceList.Normal)
		{
			return diceRoll;
		}
		else if (dicelist == diceList.Small)
		{
			return (diceRoll % 3);
		}
		else if (dicelist == diceList.Big)
		{
			return (diceRoll % 3) + 3;
		}
		else if (dicelist == diceList.Odd)
		{
			if ((diceRoll % 3) == 0)
			{
				return (diceRoll % 3);
			}
			else if ((diceRoll % 3) == 1)
			{
				return (diceRoll % 3) + 1;
			}
			else
			{
				return (diceRoll % 3) + 2;
			}
		}
		else
		{
			if ((diceRoll % 3) == 0)
			{
				return (diceRoll % 3) + 1;
			}
			else if ((diceRoll % 3) == 1)
			{
				return (diceRoll % 3) + 2;
			}
			else
			{
				return (diceRoll % 3) + 3;
			}
		}
	}
}
