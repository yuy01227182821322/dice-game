using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using System.Linq;
using System;
using UnityEditor;
using static Masu;
using static Normal;
using static Branch;
using static Start;
using static End;
using static Null;
using static Recovery;
using static Damage;
using static XMLLoader;
//using static TagHelper;

public struct ID
{
    public int rootID;
    public int masuID;
    
    public ID(int masu, int root)
    {
        masuID = masu;
        rootID = root;
    }

    public static bool operator ==(ID lID, ID rID)
    {
        bool result = false;
        result = lID.masuID == rID.masuID;
        result &= lID.rootID == rID.rootID;
        return result;
    }
    public static bool operator !=(ID lID, ID rID)
    {
        bool result = false;
        result = lID.masuID != rID.masuID;
        result |= lID.rootID != rID.rootID;
        return result;
    }
}
public struct RootState
{
    public int id;                      // ID
    public int masuNum;                 // ルートに含まれるマスの数
    public int connectBranch1;          // 自身がつながっているブランチのID
    public int connectBranch2;          // 自身がつながっているブランチのID
    public List<MasuState> masuList;    // 自身が持っているマスのリスト
}

public struct MasuState
{
    public int id;          // ID
    public Vector3 pos;     // マスのポジション
    public MasuType type;   // マスのタイプ
    public List<string> num;   // マスが持っている数値データ最大4つ

}

public class MapMng:MonoBehaviour
{
    // マップの情報
    private struct MapState
    {
        int allRootsNum;

    }
    // マップ情報のリスト
    private List<List<Masu>> mapData_= new List<List<Masu>>();
    private List<RootState> rootData_ = new List<RootState>();
    private List<GameObject> chipList=new List<GameObject>();

    // ルートの情報
    List<RootState> rootStates_=new List<RootState>();

    private ID startID_;
    private ID endID_;

    private XMLLoader xml = new XMLLoader();
    //private TagHelper tagHelper = new TagHelper();

    GameObject chips_;

    // rootObjがあるときはその下に生成できるようにする
    [SerializeField]
    private Transform rootObj_;

	// 初期化
	public void Awake()
    {
        chips_ = new GameObject();
        if(rootObj_ != null)
        {
            chips_.transform.parent = rootObj_;
        }
        chips_.name = "chips";
        var x = Resources.Load("MapData/cave", typeof(TextAsset)) as TextAsset;
        xml.LoadXml(x.text, ref rootStates_);
        // タグの追加
        for(int a=0;a<xml.MaxRoot;a++)
        {
            //tagHelper.AddTag("root" + a);
            Debug.Log("root" + a + "のタグを生成");
        }
        // チップの生成
        foreach(var root in rootStates_)
        {
            rootData_.Add(root);
            mapData_.Add(new List<Masu>());
            foreach(var masu in root.masuList)
            {
                SetChip(masu,root);
            }
        }
		foreach(var masu in mapData_[0])
		{

			if(masu.Type == MasuType.BRANCH_MASU)
			{
				foreach(var rootData in rootData_)
				{
					(masu as BranchMasu).SetRootData(rootData.id, rootData.masuNum, rootData.connectBranch1, rootData.connectBranch2);
				}
			}
		}
		SetTile();
	}

    public void Update()
    {
    }


    // マップ作製
    // x:横の最大マス数
    // y:縦の最大マス数
    public bool CreateMap()
    {
        return false;
    }


    void SetChip(MasuState masu,RootState root)
    {

        Func<MasuType, Masu> selClass = type =>
        {
            switch (type)
            {
                case MasuType.START:
                    return new Start();
                case MasuType.END:
                    return new End();
                case MasuType.NORMAL:
                    return new Normal();
                case MasuType.BRANCH:
                    return new Branch();
                case MasuType.DAMAGE:
                    return new Damage();
                case MasuType.RECOVERY:
                    return new Recovery();
                case MasuType.BRANCH_MASU:
                    return new BranchMasu();
                case MasuType.ITEM:
                    return new Item();
                case MasuType.ITEM_LOST:
                    return new ItemLost();
                case MasuType.REDICE:
                    return new ReDice();
                case MasuType.FRONT_MOVE:
                    return new FrontMove();
                case MasuType.BACK_MOVE:
                    return new BackMove();
                case MasuType.NULL:
                default:
                    return new Null();
            }
        };
        Masu data=selClass(masu.type);
        data.Pos = masu.pos;
        data.Type = masu.type;
        data.RootNumver = root.id;
        data.ID = masu.id;
        data.Num = masu.num.ConvertAll(s => int.Parse(s) );
        LoadChip(masu.pos,ref data);
        data.SetUp();
        mapData_[root.id].Add(data);
        if(masu.type==MasuType.START)
        {
            startID_.masuID = masu.id;
            startID_.rootID = root.id;
        }
        if(masu.type==MasuType.END)
        {
            endID_.masuID = masu.id;
            endID_.rootID = root.id;
        }
        //if (masu.type == MasuType.BRANCH_MASU)
        //{
        //    foreach (var rootData in rootData_)
        //    {
        //        (data as BranchMasu).SetRootData(rootData.id, rootData.connectBranch1, rootData.connectBranch2);
        //    }
        //}
        Debug.Log("2");

    }
    void LoadChip(Vector3 pos,ref Masu masu)
    {
        if(masu.Type==MasuType.BRANCH_MASU)
        {
            SetBranch(pos, masu);
            return;
        }
        var handle= Addressables.LoadAssetAsync<GameObject>("Assets/ChipAsset/Chip 1.prefab");
        handle.WaitForCompletion();
        Debug.Log(handle.Status);
        if (handle.Status == AsyncOperationStatus.Succeeded)
        {
            Debug.Log("3");

            masu.Obj = Instantiate(handle.Result, pos, Quaternion.identity);
            masu.Obj.transform.parent = chips_.transform;
            Debug.Log("size" + mapData_.Count);
            return;
        }

        Debug.Log("size" + mapData_.Count);

    }

    void SetTile()
    {
        var normalHandle = Addressables.LoadAssetAsync<GameObject>("Assets/Scenes/maptest/map 1.prefab");
        normalHandle.WaitForCompletion();
        Debug.Log("Floor Load:" + normalHandle.Status);
        if(normalHandle.Status==AsyncOperationStatus.Succeeded)
        {
            Debug.Log("タイル配置");
            if(rootObj_ != null)
            {
                var obj = Instantiate(normalHandle.Result, new Vector3(0, 0, 0), Quaternion.identity, rootObj_);
            }
            else
            {
                var obj = Instantiate(normalHandle.Result, new Vector3(0, 0, 0), Quaternion.identity);
            }
           
		}


	}

    void SetBranch(in Vector3 pos, in Masu masu)
    {
        Debug.Log("ブランチ読み込み開始");
        var handle = Addressables.LoadAssetAsync<GameObject>("Chip.prefab");
        handle.WaitForCompletion();
        Debug.Log(handle.Status);
        if(handle.Status == AsyncOperationStatus.Succeeded)
        {

            var obj = Instantiate(handle.Result, pos, Quaternion.identity);                               
            obj.transform.parent = chips_.transform;
            masu.Obj = obj;
        }
        else
		{
            Debug.Log("エラーしてるよ");
        }

}

IEnumerator LoadTile()
    {
        yield return null;
    }

    // ブランチのIDはMaxRootの+1
    public List<ID> NextIDList(ID nowID)
    {
        return mapData_[nowID.rootID][nowID.masuID - 1].NextID();
    }

    //public MasuState GetMasuByID(ID id)
    //{
    //    if(id.rootID < 0 || id.masuID <= 0)
    //    {
    //        var nullMasu = new MasuState();
    //        nullMasu.type = MasuType.NULL;
    //        return nullMasu;
    //    }
    //    return rootData_[id.rootID].masuList[id.masuID - 1];
    //}

    //// ※重いので注意して使うUpdateには使わない
    //public ID GetIDByPosition(Vector3 pos)
    //{
    //    //// 規定値
    //    //ID id = new ID();   
    //    //foreach (var root in rootData_)
    //    //{         
    //    //    foreach (var masu in root.masuList)
    //    //    {
    //    //        id.masuID += 1;
    //    //        if (masu.pos == pos)
    //    //        {
    //    //            return id;
    //    //        }
    //    //    }
    //    //    id.rootID += 1;
    //    //    id.masuID = 0;
    //    //}
    //    //// 見つからない
    //    //id.rootID = -1;
    //    //id.masuID = -1;
    //    //return id;

    //}

    public Masu GetMasuByID(ID id)
    {
        if (id.rootID < 0 || id.masuID <= 0)
        {
            return null;
        }
        return mapData_[id.rootID][id.masuID - 1];
    }

    public MasuType GetMasuTypeByID(ID id)
    {
        if (id.rootID < 0 || id.masuID <= 0)
        {
            return MasuType.NULL;
        }
        return GetMasuByID(id).Type;
    }

    public ID GetStartID()
	{
        return startID_;
	}
}
