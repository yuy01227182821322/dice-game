using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyItem : MonoBehaviour
{
	public Image[] keyItemImg_; 
	public Sprite[] sprites_;	// 0 : 未取得　1: 取得

	public void SetKeyItem(int id, bool have)
	{
		if(!have)
		{
			keyItemImg_[id].sprite = sprites_[0];
		}
		if(have)
		{
			keyItemImg_[id].sprite = sprites_[1];
		}
	}
	public bool IsHavingKeyItem(int id)
	{
		return keyItemImg_[id].sprite == sprites_[1];
	}
}
