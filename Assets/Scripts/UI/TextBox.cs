using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextBox : MonoBehaviour
{
    private Text textObj_;
	private string text_;

	private void OnEnable()
	{
		text_ = "";
		textObj_ = transform.Find("Text").GetComponent<Text>();
	}

	private void OnDisable()
	{
		Init();
	}

	public void AddText(string addText)
	{
		var tmp = text_ + addText + "\n";
		textObj_.text = tmp;
		text_ = textObj_.text;
	}

	public void SetActive(bool active)
	{
		gameObject.SetActive(active);
	}

	public void Init()
	{
		text_ = "";
		textObj_.text = "";
	}
}
