using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemList : MonoBehaviour
{
	[SerializeField]
	private GameObject buttonObj;
	[SerializeField]
	private Button closeButton_;

	private Transform buttonsParent_;

	private ItemManager selectItem_;

	private Vector2 firstPos_ = new Vector3(-250, 180, 0);
	private Vector2 x= new Vector3(500, 0, 0);
	private Vector2 y = new Vector3(0, -150, 0);

	public void SetUpAndActive(List<ItemManager> items, bool allF = false)
	{
		gameObject.SetActive(true);
		int cnt = 0;
		buttonsParent_ = transform.Find("Buttons");
		selectItem_ = null;
		foreach(var item in items)
		{
			if(!allF)
			{
				if(item.itemID >= ItemManager.ItemID.Key_1)
				{
					continue;
				}
			}
			var obj = Instantiate(buttonObj, buttonsParent_);
			obj.GetComponent<RectTransform>().anchoredPosition3D = firstPos_ + (cnt % 2 * x) + (cnt / 2 * y);
			obj.GetComponent<Button>().onClick.AddListener(()=> { ItemClick(item); });
			var text = obj.transform.Find("Text").GetComponent<Text>();
			text.text = item.name;
			cnt++;
		}
	}
	public void ItemClick(ItemManager item)
	{
		selectItem_ = item;
	}


	public void CloseButtonAddListener(UnityEngine.Events.UnityAction closeAction)
	{
		closeButton_.onClick.AddListener(closeAction);
	}

	public void Close()
	{
		gameObject.SetActive(false);
		buttonsParent_.DetachChildren();
	}

	public void OnDisable()
	{
		closeButton_.onClick.RemoveAllListeners();
	}

	public ItemManager GetSelectItem()
	{
		return selectItem_;
	}
}
