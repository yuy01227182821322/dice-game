using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChooseUI : MonoBehaviour
{
	[SerializeField]
 	private Button yesButton_;
	[SerializeField]
	private Button noButton_;

	public void Init()
	{
		yesButton_.transform.Find("Text").GetComponent<Text>().text = "Yes";
		noButton_.transform.Find("Text").GetComponent<Text>().text = "No";
		yesButton_.onClick.RemoveAllListeners();
		noButton_.onClick.RemoveAllListeners();
	}

	public void SettingAndActive(UnityEngine.Events.UnityAction yesAction, UnityEngine.Events.UnityAction noAction)
	{
		gameObject.SetActive(true);
		yesButton_.onClick.AddListener(yesAction);
		noButton_.onClick.AddListener(noAction);
	}
	public void SetChoseObjText(string yesText, string noText)
	{
		yesButton_.transform.Find("Text").GetComponent<Text>().text = yesText;
		noButton_.transform.Find("Text").GetComponent<Text>().text =noText;
	}

	public void OnDisable()
	{
		Init();
	}
}
