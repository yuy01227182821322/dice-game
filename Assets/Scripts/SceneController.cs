using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum PlayerSkin:int
{
    WHITE,
    BLACK,
    ORANGE,
    BLUE,
    UNITY,
    MAX,

}

public class SceneController : SingletonMonoBehaviour<SceneController>
{
    [System.NonSerialized]
    public int currentSceneNum = 0;
    [SerializeField]
    string[] sceneName;

    private int playerNum_;
    public int PlayerNum { get { return playerNum_; } }
    private List<PlayerSkin> playersSkin_;
    public List<PlayerSkin> PlayersSkin { get { return playersSkin_; } }

    private int winPlayerNum_ = 0;
    public int WinPlayerNum => winPlayerNum_;
    private PlayerSkin winPlayerSkin_;
    public PlayerSkin WinPlayerSkin => winPlayerSkin_;

    public void Awake()
    {
        if (this != Instance)
        {
            Destroy(gameObject);
            return;
        }

        //シーンが切り替わっても削除しないようにする処理
        DontDestroyOnLoad(gameObject);
    }
    void Start()
    {
        StartCoroutine(WaitForLoadScene());
    }

    public void NextScene()
    {
        currentSceneNum = (currentSceneNum + 1) % sceneName.Length;
        StartCoroutine(WaitForLoadScene());
    }

    IEnumerator WaitForLoadScene()
    {
        yield return SceneManager.LoadSceneAsync(sceneName[currentSceneNum]);
    }

    public void SetPlayerSettingInf(in List<PlayerSkin> playersSkin,in int playerNum)
    {
        playerNum_ = playerNum;
        playersSkin_ = playersSkin;
    }

    public void SetWinPlayer(in PlayerSkin playerSkin, in int playerNum)
    {
        winPlayerNum_ = playerNum;
        winPlayerSkin_ = playerSkin;
    }
}
