using UnityEngine;

public class ResultScene : MonoBehaviour
{
    SceneController sceneController;
    [SerializeField]
    private GameObject[] players_;
    
    // Start is called before the first frame update
    void Start()
    {
        sceneController = GameObject.Find("SceneController").GetComponent<SceneController>();
        var obj = GameObject.Instantiate(players_[(int)sceneController.WinPlayerSkin]);
        obj.transform.position = Vector3.zero;
        obj.transform.localScale = new Vector3(2,2,2);
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void OnClickButton()
    {
        sceneController.NextScene();
    }
}
