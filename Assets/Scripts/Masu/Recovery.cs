using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 回復マス
public class Recovery : Masu
{
    private int recovery_;
    private string text_;
    private float time_ = 0;
    public override bool SetUp(in Vector2Int vec, in int rootNumber, in Vector3 pos, in GameObject obj, in MasuType type)
    {
        vec_ = vec;
        rootNumber_ = rootNumber;
        pos_ = pos;
        obj_ = obj;
        type_ = type;
        return SetUp();
    }

    public override bool SetUp()
    {
        Debug.Log("setup");
        obj_.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.green);
        obj_.name = rootNumber_ + ":" + id_;

        return true;
    }
    public override List<ID> NextID()
    {
        List<ID> ids = new List<ID>();
        ID id;
        id.masuID = id_ + 1;
        id.rootID = rootNumber_;
        ids.Add(id);
        id.masuID = id_ - 1;
        ids.Add(id);
        return ids;
    }
    public override bool EventUpDate(ref EventParam eventParam)
    {
        time_ += Time.deltaTime;
        if(time_ > 3)
        {
            isEnd_ = true;
        }
        return true;
    }

    public override bool EventStart(EventParam eventParam)
    {
        Debug.Log("回復");
        text_ = "";
        recovery_ = num_[0] * 10;
        eventParam.player.Damage(-recovery_);
        Debug.Log("プレイヤー残り体力" + eventParam.player.HP);
        return true;
    }

    public override string GetText()
    {
        return "回復マスだ！\n" + recovery_ + "HP回復した";
    }

}
