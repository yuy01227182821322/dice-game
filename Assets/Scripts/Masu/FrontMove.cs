using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrontMove : Masu
{
    private ForcedMove forcedMove_;
    private int front_;
    public override bool SetUp(in Vector2Int vec, in int rootNumber, in Vector3 pos, in GameObject obj, in MasuType type)
    {
        vec_ = vec;
        rootNumber_ = rootNumber;
        pos_ = pos;
        obj_ = obj;
        type_ = type;
        return SetUp();
    }

    public override bool SetUp()
    {
        Debug.Log("setup");
        obj_.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.green);
        obj_.name = rootNumber_ + ":" + id_;

        return true;
    }
    public override List<ID> NextID()
    {
        List<ID> ids = new List<ID>();
        ID id;
        id.masuID = id_ + 1;
        id.rootID = rootNumber_;
        ids.Add(id);
        id.masuID = id_ - 1;
        ids.Add(id);
        return ids;
    }
    public override bool EventUpDate(ref EventParam eventParam)
    {
        forcedMove_.MoveUpdate(ref eventParam);
        if(forcedMove_.IsEnd)
        {
            eventParam.player.MovingDIR.Clear();
            isEnd_ = true;
        }
        return true;
    }
    public override bool EventStart(EventParam eventParam)
    {
        front_ = num_[0];
        forcedMove_ = new ForcedMove(ref eventParam, (uint)front_);
        forcedMove_.MoveStart();
        return true;
    }

    public override string GetText()
    {
        return front_ + "�}�X�i��";
    }

}
