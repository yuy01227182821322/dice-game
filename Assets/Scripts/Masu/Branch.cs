using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 分岐に行くマス
public class Branch : Masu
{
    private float time_ = 0;
    public override bool SetUp(in Vector2Int vec, in int rootNumber, in Vector3 pos, in GameObject obj, in MasuType type)
    {
        vec_ = vec;
        rootNumber_ = rootNumber;
        pos_ = pos;
        obj_ = obj;
        type_ = type;
        return SetUp();
    }

    public override bool SetUp()
    {
        Debug.Log("setup");
        obj_.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.white);
        obj_.name = rootNumber_ + ":" + id_;
        return true;
    }
    public override List<ID> NextID()
    {
        List<ID> ids=new List<ID>();
        ID id;
        id.rootID = 0;
        id.masuID = num_[0];
        ids.Add(id);
        id.rootID = rootNumber_;
        id.masuID = id_ == 1 ? id_ + 1 : id_ - 1;
        ids.Add(id);
        return ids;
    }
    public override bool EventUpDate(ref EventParam eventParam)
    {
        time_ += Time.deltaTime;
        if(time_ > 5 || Input.GetKeyUp(0))
		{
            isEnd_ = true;
		}
        return true;
    }

    public override string GetText()
    {
        return "何も起こらなかった...";
    }
}
