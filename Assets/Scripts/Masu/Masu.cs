using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MasuType
{
    NULL,           // エラー
    BRANCH_MASU,    // 分岐マス
    START,          // スタートマス
    END,            // 終了マス
    BRANCH,         // 分岐マスに続くマス
    DAMAGE,         // HPを減らす
    RECOVERY,       // HPを回復する
    NORMAL,         // 通常マス
    ITEM,           // アイテムを取得する
    REDICE,         // ダイスを振りなおす
    FRONT_MOVE,     // 前に進む
    BACK_MOVE,      // 後ろに下がる
    ITEM_LOST,      // アイテムを失う
}

// マス関連の基底クラス
public abstract class Masu
{
    // 保持情報
    // ID
    protected int id_;
    protected Vector2Int vec_;
    // 自身の所属しているルート番号
    protected int rootNumber_;
    // 位置座標
    protected Vector3 pos_;
    // 自身のゲームオブジェクト
    protected GameObject obj_;
    // マスのタイプ
    protected MasuType type_;
    // この先分岐するか
    protected int isBranch_;
    // callback用型
    delegate bool callback(GameObject obj);
    // MasuTypeごとの効果関数
    callback effectFunc_;
    // 
    protected List<int> num_;
    // マスのイベントが終了したか
    protected bool isEnd_ = false;

    //-純粋仮想----------------------------------------------------------------
    // まとめて初期化
    abstract public bool SetUp(in Vector2Int vec, in int rootNumber, in Vector3 pos, in GameObject obj, in MasuType type);
    // 代入されてる前提の初期化
    abstract public bool SetUp();
    // 自身からいけるマスのIDを返す
    abstract public List<ID> NextID();
    // イベントのアップデート一連の処理が終わるとtrueが返ってくる
    virtual public bool EventUpDate(ref EventParam eventParam)
    {
        isEnd_ = true;
        return true;
    }
    // イベント開始時に呼ぶ
    virtual public bool EventStart(EventParam eventParam)
    {
        isEnd_ = false;
        return true;
    }
    // マスイベントのテキストを取得
    abstract public string GetText();
    //-------------------------------------------------------------------------
    // getset系
    public Vector2Int Vec { get { return this.vec_; } set { this.vec_ = value; } }
    public int RootNumver { get { return this.rootNumber_; } set { this.rootNumber_ = value; } }
    public Vector3 Pos { get { return this.pos_; } set { this.pos_ = value; } }
    public GameObject Obj { get { return this.obj_; } set { this.obj_ = value; } }
    public MasuType Type { get { return this.type_; }set { this.type_ = value; } }
    public int IsBranch { get { return this.isBranch_; } set { this.isBranch_ = value; } }
    public int ID { get { return this.id_; } set { this.id_ = value; } }
    public List<int> Num { get { return this.num_;}set { this.num_=value; } }
    public bool IsEnd => isEnd_;


}
