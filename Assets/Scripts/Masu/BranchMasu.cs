using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Masu;
public class BranchMasu : Masu
{
    private struct RootData
    {
        public int id;                      // ID
        public int masuNum;                 // ルートに含まれるマスの数
        public int connectBranch1;          // 自身がつながっているブランチのID
        public int connectBranch2;          // 自身がつながっているブランチのID
    }
    private float time = 0;
    private List<RootData> rootData_=new List<RootData>();
    public override bool SetUp(in Vector2Int vec, in int rootNumber, in Vector3 pos, in GameObject obj, in MasuType type)
    {
        vec_ = vec;
        rootNumber_ = rootNumber;
        pos_ = pos;
        obj_ = obj;
        type_ = type;
        return SetUp();
    }

    public override bool SetUp()
    {
        Debug.Log("setup");
        obj_.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.white);
        obj_.name = rootNumber_ + ":" + id_;

        return true;
    }
    public override List<ID> NextID()
    {
        List<ID> ids = new List<ID>();
        ID id;
        foreach(var num in num_)
        {
            id.rootID = num;
            id.masuID = rootData_[num].connectBranch1==id_?1:rootData_[num].connectBranch2==id_?rootData_[num].masuNum:0;
            ids.Add(id);
        }
        return ids;
    }

    public override bool EventUpDate(ref EventParam eventParam)
    {
        time += Time.deltaTime;
        if(time > 5 || Input.GetKeyUp(0))
        {
            isEnd_ = true;
        }
        return true;
    }

    public override string GetText()
    {
        return "何も起こらなかった...";
    }

    public void SetRootData(int id,int masuNum, int connectBranch1,int connectBranch2)
    {
        RootData data = new RootData();
        data.id = id;
        data.masuNum = masuNum;
        data.connectBranch1 = connectBranch1;
        data.connectBranch2 = connectBranch2;
        rootData_.Add(data);
    }
}
