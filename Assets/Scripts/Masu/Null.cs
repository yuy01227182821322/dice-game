using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// エラー時のマス
public class Null : Masu
{
    public override bool SetUp(in Vector2Int vec, in int rootNumber, in Vector3 pos, in GameObject obj, in MasuType type)
    {
        vec_ = vec;
        rootNumber_ = rootNumber;
        pos_ = pos;
        obj_ = obj;
        type_ = type;
        return SetUp();
    }

    public override bool SetUp()
    {
        Debug.LogError("Null");
        //obj_.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.clear);
        //obj_.tag = "root" + rootNumber_;
        //obj_.name = rootNumber_ + ":" + id_;

        return true;
    }
    public override List<ID> NextID()
    {
        List<ID> id = new List<ID>();
        return id;
    }
	public override string GetText()
	{
        return "何も起こらなかった";
	}

}
