using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemLost : Masu
{
    private float time_ = 0;
    private string text_;
    public override bool SetUp(in Vector2Int vec, in int rootNumber, in Vector3 pos, in GameObject obj, in MasuType type)
    {
        vec_ = vec;
        rootNumber_ = rootNumber;
        pos_ = pos;
        obj_ = obj;
        type_ = type;
        return SetUp();
    }

    public override bool SetUp()
    {
        Debug.Log("setup");
        obj_.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.grey);
        obj_.name = rootNumber_ + ":" + id_;

        return true;
    }
    public override List<ID> NextID()
    {
        List<ID> ids = new List<ID>();
        ID id;
        id.masuID = id_ + 1;
        id.rootID = rootNumber_;
        ids.Add(id);
        id.masuID = id_ - 1;
        ids.Add(id);
        return ids;
    }
    public override bool EventStart(EventParam eventParam)
    {
        time_ = 0;
        text_ = "ロストマスだ！\nいずれかのアイテムを失った";
        if(eventParam.player.Inventory.GetInventoryCount() == 0)
        {
            text_ = "ロストマスだ！\nアイテムを持っていなかった";
            return true;
		}
        int rand = Random.Range(0, eventParam.player.Inventory.GetInventoryCount());
        var lostItem = eventParam.player.Inventory.items[rand];
        if(lostItem.itemID >= ItemManager.ItemID.Key_1)
		{
            eventParam.player.KeyObj.SetKeyItem((int)lostItem.itemID, false);
		}
        eventParam.player.Inventory.Removed(lostItem);
        return true;
    }
    public override bool EventUpDate(ref EventParam eventParam)
    {
        time_ += Time.deltaTime;
        if(time_ > 3 || Input.GetKeyUp(0))
        {
            isEnd_ = true;
        }
        return true;
    }

    public override string GetText()
    {
        return text_;
    }

}
