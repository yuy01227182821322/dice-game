using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : Masu
{
    private float time_ = 0;
    private string text_;
    private ChooseUI chooseUI_;
    private TextBox textBox_;
    private ItemManager item_;
    private ItemList itemListUI_;
    private EventParam eventParam_;

    private bool chooseFlag_ = true;
    public override bool SetUp(in Vector2Int vec, in int rootNumber, in Vector3 pos, in GameObject obj, in MasuType type)
    {
        vec_ = vec;
        rootNumber_ = rootNumber;
        pos_ = pos;
        obj_ = obj;
        type_ = type;
        return SetUp();
    }

    public override bool SetUp()
    {
        Debug.Log("setup");
        obj_.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.green);
        obj_.name = rootNumber_ + ":" + id_;

        return true;
    }
    public override List<ID> NextID()
    {
        List<ID> ids = new List<ID>();
        ID id;
        id.masuID = id_ + 1;
        id.rootID = rootNumber_;
        ids.Add(id);
        id.masuID = id_ - 1;
        ids.Add(id);
        return ids;
    }

    public override bool EventStart(EventParam eventParam)
    {
        eventParam_ = eventParam;
        chooseFlag_ = false;
        var game = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameCtr>();
        textBox_ = game.GetTextBox();
        var itemList = game.itemList_;
        item_ = itemList[Random.Range(0, 7)];// 0〜7までのランダム 最後の3つはキーアイテム
        text_ = "アイテムマス　　" + item_.name + "を手に入れた\n";
        if(eventParam.player.Inventory.GetInventoryCount() >= 6)
        {
            // アイテム入れ替え
            text_ += "持ち物が限界ですアイテムを入れ替えますか？";
            var canvas = GameObject.FindGameObjectWithTag("Canvas");
            chooseUI_ = canvas.transform.Find("ChooseUI").GetComponent<ChooseUI>();
            chooseUI_.SettingAndActive(ChangeYes, ChangeNo);
            itemListUI_ = canvas.transform.Find("ItemList").GetComponent<ItemList>();
            chooseFlag_ = true;
        }
        else
        {
            // アイテム取得
            eventParam.player.Inventory.Add(item_);
            if(item_.itemID >= ItemManager.ItemID.Key_1)
			{
                int id = item_.itemID - ItemManager.ItemID.Key_1;
                if(!eventParam_.player.KeyObj.IsHavingKeyItem(id))
                {
                    eventParam.player.KeyObj.SetKeyItem(id, true);
                }
                else
				{
                    text_ += "すでにこのキーアイテムは持っているようだ";
                }
            }
        }
        return true;
    }
    public override bool EventUpDate(ref EventParam eventParam)
    {
        if(chooseFlag_)
		{
            var selectItem = itemListUI_.GetSelectItem();
            if(selectItem != null)
			{
                textBox_.Init();
                textBox_.AddText("アイテムを入れ替えました\n");
                eventParam.player.Inventory.Removed(selectItem);
                eventParam.player.Inventory.Add(item_);
                chooseFlag_ = false;
                itemListUI_.Close();
            }
            return true;
		}
        time_ += Time.deltaTime;
        if(time_ > 2)
        {
            isEnd_ = true;
        }
        return true;
    }

    public override string GetText()
    {
        return text_;
    }

    private void ChangeYes()
	{
        textBox_.Init();
		textBox_.AddText("何と入れ替えますか?");
        itemListUI_.SetUpAndActive(eventParam_.player.Inventory.items);
        itemListUI_.CloseButtonAddListener(Close);
        chooseUI_.gameObject.SetActive(false);
    }
    private void ChangeNo()
    {
        textBox_.Init();
        textBox_.AddText(item_.name + "を捨てました");
        chooseUI_.gameObject.SetActive(false);
        chooseFlag_ = false;
    }

    private void Close()
	{
        ChangeNo();
    }
}
