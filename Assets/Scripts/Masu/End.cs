using System.Collections;
using System.Collections.Generic;
using UnityEngine.AddressableAssets;
using UnityEngine;

// 終了マス
public class End : Masu
{
    private string text_;
    private float time_ = 0;
    private SceneController sceneController_;
    public override bool SetUp(in Vector2Int vec, in int rootNumber, in Vector3 pos, in GameObject obj, in MasuType type)
    {
        vec_ = vec;
        rootNumber_ = rootNumber;
        pos_ = pos;
        obj_ = obj;
        type_ = type;
        return SetUp();
    }

    public override bool SetUp()
    {
        Debug.Log("setup");
        obj_.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.yellow);
        obj_.name = rootNumber_ + ":" + id_;
        var mat = Addressables.LoadAssetAsync<Material>("Assets/ChipAsset/GoalMat.mat");
        mat.WaitForCompletion();
        Obj.GetComponent<Renderer>().material = mat.Result;
        return true;
    }
    public override List<ID> NextID()
    {
        List<ID> id = new List<ID>();
        return id;
    }
    public override bool EventUpDate(ref EventParam eventParam)
    {
        time_ += Time.deltaTime;
        if(time_ > 3 && !isEnd_)
        {
            isEnd_ = true;
            sceneController_.NextScene();
        }
        return true;
    }
    public override bool EventStart(EventParam eventParam)
    {
        text_ = "キーアイテムがまだそろっていないようだ";
        // アイテムがそろっているかどうか
        bool collectItems = true;
        for(int i = 0; i < 3; i++)
        {
            if(!eventParam.player.KeyObj.IsHavingKeyItem(i))
            {
                collectItems = false;
            }
	    }

        if(collectItems)
        {
            sceneController_ = GameObject.Find("SceneController").GetComponent<SceneController>();
            sceneController_.SetWinPlayer(eventParam.player.PlayerSkin, eventParam.player.PlayerID_);
       
            text_ = "ゴールマスだ！\n脱出成功！";
        }
        else
		{
            ID startID = eventParam.mapMng.GetStartID();
            eventParam.player.SetIdAndPosision(eventParam.mapMng.GetMasuByID(startID).Pos, startID);
            text_ = "ゴールマスだ！\nキーアイテムが揃っていないようだ\nスタートに戻る";

        }
        return true;
    }

    public override string GetText()
    {
        return text_;
    }

}
