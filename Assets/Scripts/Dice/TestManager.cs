using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestManager : MonoBehaviour
{
    public GameObject[] dicePrefab = new GameObject[5];
    public GameObject explosion;
    public int diceType = 0;

    private GameObject expObj;
    private TestRoll dice;
    private bool deleteFlag = false;
    private int cnt = 400;
    private int diceResult;

    // Start is called before the first frame update
    void Start()
    {
        // 生成位置
        Vector3 pos = new Vector3(0.0f, 0.0f, 0.0f);
        // プレハブを指定位置に生成
        var obj = Instantiate(dicePrefab[diceType], pos, Quaternion.identity);
        dice = obj.GetComponent<TestRoll>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            // 生成位置
            Vector3 pos = new Vector3(0.0f, 0.0f, 0.0f);
            // プレハブを指定位置に生成
            var obj = Instantiate(dicePrefab[diceType], pos, Quaternion.identity);
            dice = obj.GetComponent<TestRoll>();
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            diceResult = Random.Range(0, 6);
            Debug.Log(diceResult);
            // 生成位置
            Vector3 pos = new Vector3(0.0f, 0.0f, 0.0f);
            // プレハブを指定位置に生成
            expObj = Instantiate(explosion, pos, Quaternion.identity);
            deleteFlag = true;
        }
        if(deleteFlag)
        {
            cnt--;
            if (cnt < 0)
            {
                deleteFlag = false;
                cnt = 400;
            }
        }
        else
        {
            Destroy(expObj);
        }
    }

    public int GetDiceRoll()
    {
        if (dice.GetDiceType() == TestRoll.DiceList.Normal)
        {
            return diceResult;
        }
        else if (dice.GetDiceType() == TestRoll.DiceList.Low)
        {
            return (diceResult % 3);
        }
        else if (dice.GetDiceType() == TestRoll.DiceList.High)
        {
            return (diceResult % 3) + 3;
        }
        else if (dice.GetDiceType() == TestRoll.DiceList.Odd)
        {
            if ((diceResult % 3) == 0)
            {
                return (diceResult % 3);
            }
            else if ((diceResult % 3) == 1)
            {
                return (diceResult % 3) + 1;
            }
            else
            {
                return (diceResult % 3) + 2;
            }
        }
        else
        {
            if ((diceResult % 3) == 0)
            {
                return (diceResult % 3) + 1;
            }
            else if ((diceResult % 3) == 1)
            {
                return (diceResult % 3) + 2;
            }
            else
            {
                return (diceResult % 3) + 3;
            }
        }
    }
}
