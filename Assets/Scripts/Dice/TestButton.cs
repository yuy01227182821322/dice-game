using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestButton : MonoBehaviour
{

    public ItemManager.ItemID buttonList;
    public int diceType;

    public int GetDiceTipe()
    {
        return diceType;
    }

    public ItemManager.ItemID GetButtonList()
    {
        return buttonList;
    }
}
