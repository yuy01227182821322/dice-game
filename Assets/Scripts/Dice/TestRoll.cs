using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestRoll : MonoBehaviour
{
    public enum DiceList
    {
        Normal,
        Low,
        High,
        Odd,
        Even
    };

    public DiceList dicelist;

    private Vector3[] dice = new[] { new Vector3(0, 180, 0), new Vector3(0, 90, 90), new Vector3(90, 0, 90),
                             new Vector3(90, 0, -90), new Vector3(-90, 0, 0), new Vector3(0, 0, -90) };
    private Vector3[] rot = new[] { new Vector3(1, 0, 1), new Vector3(0, 1, 1), new Vector3(1, 1, 0),
                            new Vector3(1, 0, -1), new Vector3(0, -1, 1), new Vector3(-1, 1, 0),
                            new Vector3(-1, 0, 1), new Vector3(0, 1, -1), new Vector3(1, -1, 0)};

    private bool flag = true;
    private int cnt = 200;
    private int roll;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if(flag)
            {
                flag = false;
            }
            else
            {
                flag = true;
            }
        }

        if(flag)
        {
            cnt--;
            if (cnt < 0)
            {
                roll = Random.Range(0, 6);
                cnt = 200;
            }
            transform.Rotate(rot[roll]);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public DiceList GetDiceType()
    {
        return dicelist;
    }
}
