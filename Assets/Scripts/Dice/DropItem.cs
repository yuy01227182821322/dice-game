using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropItem : MonoBehaviour
{
    public TestInventory testInventory;
    public ItemManager item;

    public void PicUp()
    {
        testInventory.Add(item);
    }

    public void DropOff()
    {
        //TestInventory.instance.Removed(TestInventory.instance.items[0]);
        testInventory.Removed(item);
    }
}
