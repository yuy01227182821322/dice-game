using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Test : MonoBehaviour
{
    public GameObject[] buttonPrefab = new GameObject[5];
    public GameObject[] dicePrefab = new GameObject[5];

    private int buttonTipe = 0;
    private TestButton button;
    private GameObject dice;

    private void Update()
    {
        button = buttonPrefab[buttonTipe].GetComponent<TestButton>();

        if (Input.GetKeyDown(KeyCode.R))
        {
            Debug.Log(button.GetButtonList());
        }
    }

    public void OnClick(int num)
    {
        Vector3 pos = new Vector3(0.0f, 0.0f, 0.0f);
        switch (num)
        {
            case 0:
                Destroy(dice);
                buttonTipe = 0;
                dice = Instantiate(dicePrefab[buttonTipe], pos, Quaternion.identity);
                break;
            case 1:
                Destroy(dice);
                buttonTipe = 1;
                dice = Instantiate(dicePrefab[buttonTipe], pos, Quaternion.identity);
                break;
            case 2:
                Destroy(dice);
                buttonTipe = 2;
                dice = Instantiate(dicePrefab[buttonTipe], pos, Quaternion.identity);
                break;
            case 3:
                Destroy(dice);
                buttonTipe = 3;
                dice = Instantiate(dicePrefab[buttonTipe], pos, Quaternion.identity);
                break;
            case 4:
                Destroy(dice);
                buttonTipe = 4;
                dice = Instantiate(dicePrefab[buttonTipe], pos, Quaternion.identity);
                break;
            default:
                break;
        }
    }
}
