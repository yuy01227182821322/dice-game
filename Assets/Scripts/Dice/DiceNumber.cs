using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DiceNumber : MonoBehaviour
{
    public Sprite[] numberImage = new Sprite[9];
    public GameObject obj;
    public Image image;

    private DiceManager diceManager;
    private bool diceFlag = false;
    private int cnt = 700;

    // Start is called before the first frame update
    void Start()
    {
        diceManager = obj.GetComponent<DiceManager>();
        image = image.GetComponent<Image>();
        image.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(diceFlag)
        {
            if(cnt < 500)
            {
                image.enabled = true;
                image.sprite = numberImage[diceManager.GetDiceResult()];
            }

            if(cnt < 0)
            {
                diceFlag = false;
                gameObject.SetActive(false);
                image.enabled = false;
                cnt = 700;
            }
            cnt--;
        }
    }

    public void Active()
	{
        diceFlag = true;
        this.gameObject.SetActive(true);
    }
}
