using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestInventory
{
    public List<ItemManager> items = new List<ItemManager>();
    public const int max = 6;

    public void Add(ItemManager item)
    {    
        items.Add(item);
        items.Sort((a, b)=> a.itemID - b.itemID);
    }

    public void Removed(ItemManager item)
    {
        items.Remove(item);
    }

    public int GetInventoryCount()
	{
        return items.Count;
	}
}
