using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "ScriptableObject/Create Item")]

public class ItemManager : ScriptableObject
{
    public enum ItemID
    {
        Low_Dice,
        High_Dice,
        Odd_Dice,
        Even_Dice,
        Key_1,
        Key_2,
        Key_3
    }
    public ItemID itemID;
    new public string name = "New Item";
}
