using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
	private bool endFlag_ = false;
	public bool EndFlag => endFlag_;
	int cnt;
	// エフェクト終了時
	private void Update()
	{
		cnt++;
		if(cnt > 400)
		{
			endFlag_ = true;
		}
	}
}
