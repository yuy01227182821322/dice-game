using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TestItem : MonoBehaviour
{
    public GameObject[] buttonPrefab = new GameObject[5];

    private List<GameObject> listObj = new List<GameObject>();
    private List<ItemManager.ItemID> list = new List<ItemManager.ItemID>();
    private int cnt = 0;

    // Start is called before the first frame update
    void Start()
    {
        Vector3 pos = new Vector3(0.0f, 120.0f, 0.0f);
        var obj = Instantiate(buttonPrefab[0], pos, Quaternion.identity);
        obj.transform.parent = GameObject.Find("Content").transform;
        obj.GetComponent<RectTransform>().anchoredPosition = pos;

        list.Add(ItemManager.ItemID.Low_Dice);
        list.Add(ItemManager.ItemID.High_Dice);
        list.Add(ItemManager.ItemID.Even_Dice);
        list.Add(ItemManager.ItemID.High_Dice);
        list.Add(ItemManager.ItemID.Odd_Dice);
        list.Add(ItemManager.ItemID.Even_Dice);

        foreach (ItemManager.ItemID itemID in list)
        {
            switch(itemID)
            {
                case ItemManager.ItemID.Low_Dice:
                    if (list.Contains(ItemManager.ItemID.Low_Dice))
                    {
                        listObj.Add(Instantiate(buttonPrefab[1], pos, Quaternion.identity));
                    }
                    break;
                case ItemManager.ItemID.High_Dice:
                    if (list.Contains(ItemManager.ItemID.High_Dice))
                    {
                        listObj.Add(Instantiate(buttonPrefab[2], pos, Quaternion.identity));
                    }
                    break;
                case ItemManager.ItemID.Odd_Dice:
                    if (list.Contains(ItemManager.ItemID.Odd_Dice))
                    {
                        listObj.Add(Instantiate(buttonPrefab[3], pos, Quaternion.identity));
                    }
                    break;
                case ItemManager.ItemID.Even_Dice:
                    if (list.Contains(ItemManager.ItemID.Even_Dice))
                    {
                        listObj.Add(Instantiate(buttonPrefab[4], pos, Quaternion.identity));
                    }
                    break;
            }
            BottunSeting(pos);
        }

    }

    private void BottunSeting(Vector3 pos)
    {
        float offset = 30;
        cnt++;
        listObj.Last().transform.parent = GameObject.Find("Content").transform;
        listObj.Last().GetComponent<RectTransform>().anchoredPosition = new Vector3(pos.x, pos.y - (offset * cnt), pos.z);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
