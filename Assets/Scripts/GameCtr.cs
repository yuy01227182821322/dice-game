using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AddressableAssets;
using UnityEngine.Assertions;
using UnityEngine.ResourceManagement.AsyncOperations;
using static MapMng;
public class GameCtr : MonoBehaviour
{
    [SerializeField]
    private GameObject[] PlayerPrefab;

    [SerializeField]
    private TextBox textBox_;

    public ItemManager[] itemList_;
    public Transform playerParent_;
    public CameraCtr camObj;

    private EventCtr EventCtr_;
    private MapMng mapMng_;

    // ボタン位置テーブル(各方向分)
    private Vector3[] buttonTable = {new Vector3(-300, 0),  // LEFT
                                     new Vector3(300, 0),   // RIGHT
                                     new Vector3(0, 300),   // UP
                                     new Vector3(0, -300)}; // DOWN
    private float[] buttonAngleTable = {90,  // LEFT
                                        -90,   // RIGHT
                                        0,   // UP
                                        180}; // DOWN

    // キャラUI位置テーブル(各方向分)
    private Vector3[] charUITable = {new Vector3(-770, 440),  // LEFTUP
                                     new Vector3(770, 440),   // RIGHTUP 
                                     new Vector3(-770, -440),  // LEFTDOWN
                                     new Vector3(770, -440)}; // RIGHTDOWN
    SceneController sceneController;
    // Start is called before the first frame update
    void Start()
    {
        sceneController = GameObject.Find("SceneController").GetComponent<SceneController>();
        mapMng_ = this.GetComponent<MapMng>();
        // カメラ初期位置の設定(StartPosの設定)
        camObj.Init(mapMng_.GetMasuByID(mapMng_.GetStartID()).Pos);

        List<Player> players = new List<Player>();
        // プレイヤーパラメータ表示UI
        var cUIHandle = Addressables.LoadAssetAsync<GameObject>("CharUI.prefab");
        cUIHandle.WaitForCompletion();
        var canvas = GameObject.FindGameObjectWithTag("Canvas").transform;
		for(int i = 1; i < sceneController.PlayerNum+1; i++)
		{
            // プレイヤー設定
            var playerSkin = sceneController.PlayersSkin[i - 1];
            var playerObj = Instantiate(PlayerPrefab[(int)playerSkin], playerParent_);
			var player = playerObj.GetComponent<Player>();
            var cUI = GameObject.Instantiate(cUIHandle.Result, canvas);
            cUI.transform.localPosition = charUITable[i - 1];
            player.SetCurrentID(mapMng_.GetStartID());
			player.SetUP(cUI, i, playerSkin);
            playerObj.name = i + "P";
            playerObj.transform.position = mapMng_.GetMasuByID(player.ID).Pos;
            playerObj.transform.rotation = Quaternion.Euler(0, 90, 0);
            players.Add(player);

			// プレイヤー設定後の設定
			var uiName = cUI.transform.Find("Name");
			uiName.Find("char").GetComponent<Text>().text = playerObj.name;
			cUI.name = cUI.name + playerObj.name;           
        }
		EventCtr_ = new EventCtr(ref players, ref mapMng_, ref textBox_);

        // UI作成
        var bHandle = Addressables.LoadAssetAsync<GameObject>("Assets/Scenes/masuTest/Prefab/Arrow.prefab");
        bHandle.WaitForCompletion();
        Assert.IsFalse(bHandle.Status != AsyncOperationStatus.Succeeded);
        var moveUI = canvas.Find("MoveEvent/Arrow");
        for(int i = 0; i < buttonTable.Length; i++)
        {
            var button = GameObject.Instantiate(bHandle.Result, moveUI);
            button.transform.localPosition = buttonTable[i];
            button.transform.rotation = Quaternion.Euler(0, 0, buttonAngleTable[i]);
            button.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        EventCtr_.Update();
    }

    public TextBox GetTextBox()
    {
        return textBox_;
    }
}
