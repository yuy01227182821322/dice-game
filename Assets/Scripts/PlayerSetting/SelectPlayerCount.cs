using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SelectPlayerCount : MonoBehaviour
{

    private Transform[] frame;
    [SerializeField] private GameObject controller;
    // タイトルのテキストオブジェクト
    [SerializeField] private Text title_;

    [SerializeField] private Canvas canvas_;

    [SerializeField] private RawImage charactorImage_;

    public void Awake()
    {
        gameObject.SetActive(true);
    }
    public void OnEnable()
    {
        // 定数
        const string myTitle = "何人で遊ぶ?";

        title_.text = myTitle;
    }
    public void PointerEnter(Object myObj)
    {
        (myObj as GameObject).transform.GetChild(1).gameObject.SetActive(true);
    }

    public void PointerExit(Object myObj)
    {
        (myObj as GameObject).transform.GetChild(1).gameObject.SetActive(false);
    }

    public void ButtonUp(Object myObj)
    {
        // 定数
        const string nextStateName = "SelectPlayerType";
        const string tag = "Player";

        controller.GetComponent<PlayerSettingMng>().PlayerNum = int.Parse(myObj.name);
        canvas_.transform.Find(nextStateName).gameObject.SetActive(true);
        // キャラクターの画像を消す
        charactorImage_.gameObject.SetActive(false);
        var players = GameObject.FindGameObjectsWithTag(tag);
        // 今いるキャラクターを消す
        foreach (var player in players)
        {
            player.SetActive(false);
        }
        gameObject.SetActive(false);

    }

}
