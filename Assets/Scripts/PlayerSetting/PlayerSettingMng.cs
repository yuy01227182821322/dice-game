using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;


public class PlayerSettingMng : MonoBehaviour
{
    [SerializeField] private GameObject[] players_;
    [SerializeField]private int playerNum_;
    [SerializeField] private Camera mainCamera_;
    public int PlayerNum { get { return this.playerNum_; } set { this.playerNum_ = value; } }
    // mapを保存するオブジェクト
    private GameObject mapObj_;
    private SceneController sceneController_;
    private List<PlayerSkin> playersSkin_;
    public List<PlayerSkin> PlayersSkins { set { playersSkin_ = value; } }

    public void Awake()
    {
        var tmp=Addressables.InitializeAsync();
        tmp.WaitForCompletion();
        var handle = Addressables.LoadAssetAsync<GameObject>("Assets/Scenes/maptest/map2.prefab");

        players_[UnityEngine.Random.Range(0, players_.Length)].SetActive(true);


        handle.WaitForCompletion();
        Debug.Log(handle.Status);
        if (handle.Status == AsyncOperationStatus.Succeeded)
        {
            mapObj_=Instantiate(handle.Result, new Vector3(-10,0,30), Quaternion.identity);
            return;
        }

    }
    public void Start()
    {
        sceneController_ = GameObject.Find("SceneController").GetComponent<SceneController>();

    }

    public void NextScene()
    {
        sceneController_.SetPlayerSettingInf(in playersSkin_,in playerNum_);

        sceneController_.NextScene();
    }

    public void Update()
    {
        mainCamera_.transform.RotateAround(new Vector3(0, 0, 0), new Vector3(0, 1, 0), 0.1f);
    }

}
