using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class SelectPlayerType : MonoBehaviour
{
    [SerializeField] private Text titele_;
    [SerializeField] private Text subTitele_;
    [SerializeField] private Canvas canvas_;
    [SerializeField] private GameObject controller;


    //Player配置用のいろいろ
    private const float radius_=5;
    [SerializeField] private int playerNum_;
    private List<float> arc_=new List<float> { };
    [SerializeField] private GameObject table_;

    [SerializeField] private GameObject[] charactors_;
    private Camera tableCamera_;
    private GameObject tableObj_;

    private bool LOrR_=true;
    private bool isTurn_=false;
    private float turnVol_ = 0;
    private int selChara_ = 0;
    private int selecter_ = 0;

    private List<PlayerSkin> playCharactor_ = new List<PlayerSkin>();

    public void Awake()
    {
        gameObject.SetActive(false);
    }

    public void Update()
    {
        const float turnAngle = 1f;
        if(isTurn_)
        {
            tableCamera_.transform.RotateAround(tableObj_.transform.position, new Vector3(0, 1, 0), turnAngle * (LOrR_ ? 1 : -1));
            turnVol_ += turnAngle ;
            if(turnVol_>=(360f/charactors_.Length))
            {
                isTurn_ = false;
                turnVol_ = 0f;
            }
        }

    }

    //-----------------UIボタン用------------------------------------
    public void TurnLeft()
    {
        if(!isTurn_)
        {
            LOrR_ = true;
            isTurn_ = true; 
            selChara_ = (selChara_ <= 0 ? charactors_.Length-1 : selChara_ - 1);
            Debug.Log("選択キャラ：" + selChara_);

        }
    }

    public void TurnRight()
    {
        if(!isTurn_)
        {
            LOrR_ = false;
            isTurn_ = true;
            selChara_ = (selChara_ >= charactors_.Length-1 ? 0 : selChara_ + 1);
            Debug.Log("選択キャラ：" + selChara_);
        }

    }

    public void PushEnter()
    {
        playCharactor_[selecter_] =(PlayerSkin) selChara_;
        selecter_++;
        if(selecter_>=playCharactor_.Count)
        {
            const string nextStateName = "CheckTutorial";

            controller.GetComponent<PlayerSettingMng>().PlayersSkins = playCharactor_;
            this.gameObject.SetActive(false);
            canvas_.transform.Find(nextStateName).gameObject.SetActive(true);
        }
        const string tex = "人目のキャラクターを選んでね";
        subTitele_.text = (selecter_+1) + tex;
    }
    //--------------------------------------------------------------

    public void OnEnable()
    {
        // 定数
        const string myTitle = "誰で遊ぶ？";
        const string tex = "人目のキャラクターを選んでね";
        const string lightName = "Mine Directional";
        for (int p=0;p< controller.GetComponent<PlayerSettingMng>().PlayerNum;p++)
        {
            playCharactor_.Add(0);
        }
        titele_.text = myTitle;
        CreateCharactorTable();
        GameObject.Find(lightName).SetActive(false);
        subTitele_.text = (selecter_ + 1) + tex;

    }

    private void CreateCharactorTable()
    {
        var animH = Addressables.LoadAssetAsync<RuntimeAnimatorController>("Assets/PlayerModel/orange.controller");
        tableObj_ = new GameObject("TableObj");
        tableObj_.transform.position = new Vector3(0, 0, 0);
        const int playerLayer = 6;
        const int unichanLayer = 12;
        float angle = (360f / charactors_.Length)*(Mathf.PI/180);


        // キャラクターの生成
        List<GameObject> players=new List<GameObject>();
        for(int p=0;p<charactors_.Length;p++)
        {
            Vector3 pos = new Vector3(Mathf.Cos(angle*p) * radius_, 100, Mathf.Sin(angle*p) * radius_);
            Quaternion quat = new Quaternion();
            quat.eulerAngles = new Vector3(0, 90 - (360f / charactors_.Length)*p, 0);

            players.Add(Instantiate(charactors_[p], pos, quat));
            players[p].transform.parent = tableObj_.transform;
            if(p != 4)
            {
                players[p].layer = playerLayer;
            }
            else
			{
                players[p].layer = unichanLayer;
            }
            
            if(players[p].transform.Find("body") != null)
            {
                players[p].transform.Find("body").GetComponent<SkinnedMeshRenderer>().receiveShadows = false;
                players[p].transform.Find("body").GetComponent<SkinnedMeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            }
            foreach (Transform child in players[p].transform)
            {
                child.gameObject.layer = playerLayer;
            }

        }
        animH.WaitForCompletion();
        Debug.Assert(animH.Status == AsyncOperationStatus.Succeeded);

        // キャラクターにアニメーションを追加
        foreach(var player in players)
        {
            if(player.GetComponent<Animator>() == null)
            {
                var anim = player.AddComponent<Animator>();
                anim.runtimeAnimatorController = animH.Result;
            }
            
        }

        // カメラの生成
        const string rawImageName = "TableCamera";
        tableCamera_ = new GameObject("Tablecamera").AddComponent<Camera>();
        tableCamera_.transform.position = new Vector3(radius_ + 4f, 102, 0);
        Quaternion q = new Quaternion();
        q.eulerAngles=new Vector3(0, -90, 0);
        tableCamera_.transform.rotation = q;
        tableCamera_.clearFlags = CameraClearFlags.SolidColor;
        tableCamera_.backgroundColor = Color.clear;
        tableCamera_.cullingMask = 1 << playerLayer | 1 << unichanLayer;
        var camera = table_.transform.Find(rawImageName).GetComponent<RawImage>();
        tableCamera_.targetTexture = camera.texture as RenderTexture;
        tableCamera_.transform.parent = tableObj_.transform;

        // スポットライトの生成
        Light light = new GameObject("TableLight").AddComponent<Light>();
        light.type = LightType.Spot;
        light.transform.parent = tableCamera_.transform;
        light.transform.position = tableCamera_.transform.position;
        light.transform.rotation = tableCamera_.transform.rotation;
        light.spotAngle = 60f;
        light.range = 8;
        light.intensity = 5;
    }
}
