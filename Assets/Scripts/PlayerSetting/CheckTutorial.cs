using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckTutorial : MonoBehaviour
{
    [SerializeField] private Text title_;
    [SerializeField] private Texture[] image_;
    [SerializeField] private RawImage[] rawImage_;
    private int count=0;
    private int outsideFlag_=1;
    // 画像が移動中かどうか
    private bool isMove_ = false;
    // falseが右　trueが左
    private bool moveDir_ = false;
    private float moveVal_=0;
    [SerializeField]private PlayerSettingMng controller_;
    [SerializeField] private float canvasSizeX_;
    // 定数
    private const string rightButtonName = "Right";
    private const string leftButtonName = "Left";

    public void Awake()
    {
        gameObject.SetActive(false);
        this.transform.Find(leftButtonName).gameObject.SetActive(false);

    }

    public void Update()
    {
        const float moveSpeed = 5;
        if(isMove_)
        {

            if (moveDir_)
            {

                // 左
                float moveSize = 0;

                for (int i=0;i<rawImage_.Length;i++)
                {
                    var pos =rawImage_[i].rectTransform.localPosition;

                    pos.x -= moveSpeed;
                    rawImage_[i].rectTransform.localPosition = pos;
                    moveSize = rawImage_[i].rectTransform.rect.width;

                }
                moveVal_ += moveSpeed;
                if (moveVal_ >= moveSize)
                {
                    isMove_ = false;
                    moveVal_ = 0;
                }
            }
            else
            {
                // 右
                float moveSize = 0;
                for (int i = 0; i < rawImage_.Length; i++)
                {
                    var pos = rawImage_[i].rectTransform.localPosition;
                    pos.x += moveSpeed;
                    rawImage_[i].rectTransform.localPosition = pos;
                    moveSize = rawImage_[i].rectTransform.rect.width;
                }
                moveVal_ += moveSpeed;
                if(moveVal_>=moveSize)
                {
                    isMove_ = false;
                    moveVal_ = 0;
                }
            }
            Debug.Log(outsideFlag_);
        }
    }

    public void UpLeftButton(Object button)
    {
        if(!isMove_)
        {
            count = (count <= 0 ? 0 : count - 1);
            if(count <= 0)
            {
                this.transform.Find(leftButtonName).gameObject.SetActive(false);
            }
            else
            {
                this.transform.Find(rightButtonName).gameObject.SetActive(true);
                rawImage_[outsideFlag_].texture = image_[count];
                

            }
            rawImage_[outsideFlag_].rectTransform.localPosition = new Vector3(rawImage_[outsideFlag_].rectTransform.rect.width, 0f, 0);
            isMove_ = true;
            outsideFlag_ = (outsideFlag_ == 0 ? 1 : 0);
            moveDir_ = true;
        }
        Debug.Log(count);
    }

    public void UpRightButton(Object button)
    {
        if(!isMove_)
        {
            count = (count >= image_.Length - 1 ? image_.Length - 1 : count + 1);
            if(count >= image_.Length - 1)
            {
                this.transform.Find(rightButtonName).gameObject.SetActive(false);
            }
            else
            {
                this.transform.Find(leftButtonName).gameObject.SetActive(true);
                rawImage_[outsideFlag_].texture = image_[count];
               
            }
            Vector3 tmp = rawImage_[outsideFlag_].rectTransform.localPosition;
            Vector3 tmp2 = new Vector3(-rawImage_[outsideFlag_].rectTransform.rect.width, -23f, 0); 

            rawImage_[outsideFlag_].rectTransform.localPosition = new Vector3(-rawImage_[outsideFlag_].rectTransform.rect.width,0f,0);
            isMove_ = true;
            outsideFlag_ = (outsideFlag_==0?1:0);
            moveDir_ = false;
        }
        Debug.Log(count);
    }

    public void UpEnterButton()
    {
        controller_.NextScene();

    }
    public void OnEnable()
    {
        // 定数
        const string myTitle = "チュートリアル!";
        
        title_.text = myTitle;

        rawImage_[0].texture = image_[0];

    }

}
