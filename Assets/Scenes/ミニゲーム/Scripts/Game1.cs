//using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game1 : MonoBehaviour
{
    public enum Phase
    {
        diceRoll,
        Change,
        Judgement
    };

    [SerializeField] private List<int> selectDiceList;
    [SerializeField] private List<GameObject> diceObject;
    [SerializeField] private List<GameObject> keepDiceList;
    private Game1Judment judment;
    private GameObject canvas;
    [SerializeField] private GameObject controller;
    [SerializeField] private Controller controllerScript;

    private Phase phase = Phase.diceRoll;

    // Start is called before the first frame update
    void Start()
    {
        selectDiceList = new List<int>();
        keepDiceList = new List<GameObject>();
        diceObject = new List<GameObject>();
        judment = GetComponent<Game1Judment>();
        controllerScript = controller.GetComponent<Controller>();
        canvas = GameObject.Find("Canvas");
    }

    // Update is called once per frame
    void Update()
    {
        //交換するダイスの選択
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                var a = hit.collider.gameObject.GetComponent<DiceClick>();

                var removeIndex = diceObject.FindIndex(member => member.gameObject == hit.collider.gameObject);
                DestroyObject(hit.collider.gameObject);
                selectDiceList.RemoveAt(removeIndex);
                diceObject.RemoveAt(removeIndex);
                a.ChangeSelect();
            }
        }
    }
    void OnGUI()
    {
        GUI.skin.label.fontSize = 32;
        GUI.color = Color.black;
        GUI.Label(new Rect(Screen.width - 200, 20, 200, 50), judment.GetRole().ToString());
        switch (phase)
        {
            case Phase.diceRoll:
                // ボタンを表示する
                if (GUI.Button(new Rect(20, 20, 100, 50), "Stop"))
                {
                    for (int cnt = 0; cnt < 5; cnt++)
                    {
                        var diceType = Random.Range(0, 6);
                        selectDiceList.Add(diceType);
                        selectDiceList.Sort();
                    }

                    AddDice();

                    phase = Phase.Change;
                }
                break;
            case Phase.Change:
                if (GUI.Button(new Rect(20, 70, 100, 50), "change"))
                {
                    //再抽選回数の保存
                    var listSize = 5 - diceObject.Count;
                    foreach (var obj in diceObject)
                    {
                        DestroyObject(obj);
                    }

                    diceObject = new List<GameObject>();

                    for (int cnt = 0; cnt < listSize; cnt++)
                    {
                        var diceType = Random.Range(0, 6);
                        selectDiceList.Add(diceType);
                        selectDiceList.Sort();
                    }

                    AddDice();

                    phase = Phase.Judgement;

                }
                break;
            default:
                break;
        }
    }
    public void AddDice()
    {
        var cnt = 0; 
        foreach (var member in selectDiceList)
        {
            var pos = new Vector3(1.5f * cnt - 5, 0.0f, 0.0f);
            var scale = new Vector3(0.5f, 0.5f, 0.5f);
            diceObject.Add(controllerScript.DiceGenerator(member, pos, scale));
            diceObject.Last().transform.parent = canvas.transform;
            cnt++;
        }
    }
    public List<int> GetSelectDice()
    {
        selectDiceList.Sort();
        return selectDiceList;
    }

    public Phase GetPhase()
    {
        return phase;
    }
}
