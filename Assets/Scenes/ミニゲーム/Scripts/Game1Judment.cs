using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game1Judment : MonoBehaviour
{
     public enum Role
    {
        non,
        pair,
        two_pair,
        three_card,
        four_card,
        hull_house,
        straight
    }

    [SerializeField] private Role role = Role.non;
    [SerializeField] private int diceSum = 0;
    private int muchCnt = 0;
    private Game1 game1; 
    // Start is called before the first frame update
    void Start()
    {
        role = Role.non;
        game1 = GetComponent<Game1>();
    }

    // Update is called once per frame
    void Update()
    {
        if (game1.GetPhase() != Game1.Phase.Judgement)
        {
            return;
        }
        if(role != Role.non)
        {
            return;
        }
        var targetList = game1.GetSelectDice();
        var muchCnt = 0;
        var beforeSearch = 0;
        bool chain = true;
        for(int x = 0;x<targetList.Count;x++)
        {
            diceSum += targetList[x]+1;

            if(role >= Role.three_card && targetList[x] == beforeSearch)
            {
                continue;
            }

            for(int y = x+1;y<targetList.Count;y++)
            {
                if(targetList[x]==targetList[y])
                {
                    muchCnt++;
                }
            }

            beforeSearch = targetList[x];
            role = CheckRole(muchCnt);
            muchCnt = 0;

            if(role == Role.non)
            {
                if(targetList[0] == 1 || targetList[0] == 2)
                {
                    if(x+1< targetList.Count)
                    {
                        chain = (targetList[x] + 1 == targetList[x + 1] ? true : false);
                    }
                }
            }
            else
            {
                chain = false;
            }
        }

        if(chain == true)
        {
            role = Role.straight;
        }
    }

    public Role CheckRole(int cnt)
    {
        switch (cnt)
        {
            case 1:
                return (CheckTwoPair() ? Role.two_pair : CheckHullHouse(cnt) ? Role.hull_house : Role.pair);
            case 2:
                return (CheckHullHouse(cnt)? Role.hull_house:Role.three_card);
            case 3:
                return Role.four_card;
            default:
                return role;
        }
    }

    public bool CheckTwoPair()
    {
        return (role == Role.pair ? true : false);
    }

    public bool CheckHullHouse(int cnt)
    {
        if (role == Role.pair)
        {
            return (cnt == 2 ? true : false);
        }

        if (role == Role.three_card)
        {
            return (cnt == 1 ? true : false);
        }
        return false;
    }

    public Role GetRole()
    {
        return role;
    }
    public int GetDiceScore()
    {
        return diceSum;
    }
}
