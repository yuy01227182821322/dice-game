using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Controller : MonoBehaviour
{
    public enum MiniGameType
    {
        Random,
        Game1,
        Game2
    }
    public int diceType = 0;
    public List<int> diceRoll;

    public GameObject[] miniGamePrefab;

    public GameObject dicePrefab;

    private GameObject diceObject;
    private Vector3Int[] diceFace = new[] { new Vector3Int(0, 180, 0), new Vector3Int(0, 90, 90), new Vector3Int(90, 0, 90),
                             new Vector3Int(90, 0, -90), new Vector3Int(-90, 0, 0), new Vector3Int(0, 0, -90) };

    public MiniGameType type;
    // Start is called before the first frame update
    void Start()
    {
        diceRoll = new List<int>();
        SelectMiniGameMode(type);
    }

    public void SelectMiniGameMode(MiniGameType type)
    {
        if(type == MiniGameType.Random)
        {
            miniGamePrefab[Random.Range(0, miniGamePrefab.Length)].SetActive(true);
            return;
        }

        miniGamePrefab[(int)type-1].SetActive(true);
    }

    public GameObject DiceGenerator(int diceType,Vector3 pos,Vector3 scale)
    {
        diceObject = Instantiate(dicePrefab,new Vector3(0.0f,0.0f,0.0f), Quaternion.identity);
        diceObject.transform.localRotation = Quaternion.Euler(diceFace[diceType]);
        diceObject.name = "dice" + diceType;
        DiceMovement(diceObject, pos, scale);
        return diceObject;
    }
    public GameObject DiceMovement(GameObject gameObject,Vector3 pos,Vector3 scale)
    {
        diceObject.transform.localPosition = pos;
        diceObject.transform.localScale = scale;
        return diceObject;
    }
}
