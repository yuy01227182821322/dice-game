using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game2Judment : MonoBehaviour
{
    private Game2 game2;
    [SerializeField] private int diceScore = 0;
    public void Start()
    {
        game2 = GetComponent<Game2>();
    }

    //Burstしていれば-1それ以外はスコアを返す
    public int Judment()
    {
        diceScore = 0;
        foreach (var member in game2.GetDiceList())
        {
            diceScore += (member + 1);
            if(isBurst())
            {
                return -1;
            }
        }
        return diceScore;
    }

    private bool isBurst()
    {
        return (diceScore > 13 ? true : false);
    }
}
