using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceClick : MonoBehaviour
{
    bool select = false;

    public bool isSelect()
    {
        return select;
    }

    public void ChangeSelect()
    {
        select = !select;
    }
}
