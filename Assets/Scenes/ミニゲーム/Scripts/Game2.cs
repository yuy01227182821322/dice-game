using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game2 : MonoBehaviour
{
    public enum Phase
    {
        diceRoll,
        addDice,
        Judgement
    }

    Game2Judment judment;

    [SerializeField] private GameObject controller;
    [SerializeField] private Controller controllerScript;
    [SerializeField] private Phase phase = Phase.diceRoll;
    [SerializeField] private List<int> diceList;
    [SerializeField] private List<GameObject> diceObject;

    private GameObject canvas;

    // Start is called before the first frame update
    void Start()
    {
        controllerScript = controller.GetComponent<Controller>();
        judment = GetComponent<Game2Judment>();
        canvas = GameObject.Find("Canvas");

        diceList = new List<int>();
        diceObject = new List<GameObject>();
    }

    // Update is called once per frame
    public void Update()
    {
    }
    public void AddDice()
    {
        var cnt = 0;
        foreach (var member in diceList)
        {
            var pos = new Vector3(1.5f * cnt - 5, 0.0f, 0.0f);
            var scale = new Vector3(0.5f, 0.5f, 0.5f);
            diceObject.Add(controllerScript.DiceGenerator(member, pos, scale));
            diceObject.Last().transform.parent = canvas.transform;
            cnt++;
        }
    }
    public List<int> GetDiceList()
    {
        return diceList;
    }
    private void OnGUI()
    {
        var score = judment.Judment();
        GUI.Label(new Rect(130, 20, 200, 50),(score != -1?score.ToString():"Burst"));
        switch (phase)
        {
            case Phase.diceRoll:
                if (GUI.Button(new Rect(20, 20, 100, 50), "Stop"))
                {
                    for (int i = 0; i < 2; i++)
                    {
                        diceList.Add(Random.Range(0, 6));
                    }
                    diceList.Sort();
                    AddDice();

                    phase = Phase.addDice;
                }
                break;
            case Phase.addDice:
                phase = (GUI.Button(new Rect(20, 80, 100, 50), "end") ? Phase.Judgement : Phase.addDice);

                if (GUI.Button(new Rect(20, 20, 100, 50), "Stop"))
                {
                    diceList.Add(Random.Range(0, 6));
                    diceList.Sort();
                    AddDice();
                }
                break;
            default:
                break;
        }
    }
}
