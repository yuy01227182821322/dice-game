using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class testcamera : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //camera_ = this.GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        var x = this.transform.position.x;
        var y = this.transform.position.y;
        var z = this.transform.position.z;
        if(Input.GetKey(KeyCode.RightArrow))
        {
            z -= 0.1f;
        }  
        if(Input.GetKey(KeyCode.LeftArrow))
        {
            z += 0.1f;
        }      
        if(Input.GetKey(KeyCode.UpArrow))
        {
            x += 0.1f;
        }   
        if(Input.GetKey(KeyCode.DownArrow))
        {
            x -= 0.1f;
        }
        this.transform.position = new Vector3(x, y, z);
    }
}
