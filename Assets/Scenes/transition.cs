using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class transition : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Return))
        {
            StartCoroutine(ReturnGameScene());
		}
    }

    IEnumerator ReturnGameScene()
	{
        // 前のシーンを読み込んで
        var scene = SceneManager.GetSceneByName("MasuTest");
        SceneManager.SetActiveScene(scene);
        var root = scene.GetRootGameObjects();
        foreach(var obj in root)
        {
            obj.SetActive(true);
        }
        yield return SceneManager.UnloadSceneAsync("遷移テスト");
        
    }
}
