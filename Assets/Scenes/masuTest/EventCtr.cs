using System.Collections.Generic;
public class EventCtr
{
    private List<Player> players_;
    private Event event_;
    private int nowPlayerID_;
    public EventCtr(ref List<Player> players, ref MapMng mapMng, ref TextBox textBox)
    {
        players_ = players;
        event_ = null;
        nowPlayerID_ = 0;
        EventParam eParam = new EventParam(players_[nowPlayerID_], ref mapMng, event_,ref textBox);
        EventStart(new ActionSelect(ref eParam));
    }

    public void EventStart(Event e)
    {
        event_ = e;
        if (event_ != null)
        {
            event_.EventStart();
        }
    }

    public void Update()
    {
        if (event_ == null)
        {
            return;
        }
        if (event_.IsFinished)
        {
            event_.ChangePlayer(GetNextPlayer());
        }
        event_.EventUpdate();
        var next = event_.EventParam.nextEvent;
        if (event_ != next && next != null)
        {
            EventStart(next);
        }
       
    }
    public Player GetNextPlayer()
    {
        nowPlayerID_++;
        if (nowPlayerID_ >= players_.Count)
        {
            nowPlayerID_ = 0;
        }
        return players_[nowPlayerID_];
    }
}
