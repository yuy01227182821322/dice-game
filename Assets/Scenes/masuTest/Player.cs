using DG.Tweening;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.Assertions;

public class Player : MonoBehaviour
{

    public enum DIR
    {
        LEFT,
        RIGHT,
        UP,
        DOWN,
        MAX
    }
    public enum AnimType
    {
        Idle,
        Run
    }

    [SerializeField]
    private Animator animator_;
    [SerializeField]
    private float speed = 3.0f;
    [SerializeField]
    private int hp_ = 100;
    public int HP=> hp_;
    public bool IsStoped { get { return isStoped_; } set { isStoped_ = value; } }
    private ID currentID_;
    public ID ID => currentID_;
    private ID beforeID_;   // ひとつ前にいたID
    private bool isStoped_ = false;
    private LinkedList<DIR> movingDir_ = new LinkedList<DIR>();
    public LinkedList<DIR> MovingDIR => movingDir_;
    private Text lifeObj_ = null;    // 体力オブジェクト
    private KeyItem keyObj_;     // キーアイテム表示オブジェクト
    public KeyItem KeyObj => keyObj_;   
    private TestInventory inventory_ = new TestInventory();   // 持っているアイテム
    public TestInventory Inventory => inventory_;

    // プレイヤーが何Pか
    private int playerID_ = 0;
    public int PlayerID_ => playerID_;
    // プレイヤーのスキンID
    private PlayerSkin playerSkin_;
    public PlayerSkin PlayerSkin => playerSkin_;

    // Start is called before the first frame update
    void Start()
    {
        hp_ = 100;
        beforeID_ = new ID(0, 0);
    }

    public void MoveMasu(Vector3 nextPos, ID nextID)
    {
        beforeID_ = currentID_;
        currentID_ = nextID;
        MovePoint(this.transform.position, nextPos);
        animator_.SetFloat("Speed", 3.0f);
    }

    public void MovePoint(Vector3 ownPos, Vector3 point)
    {
        var time = (point - ownPos).magnitude / speed;
        transform.DOMove(point, time, false).SetEase(Ease.Linear).OnComplete(Stop);
        transform.LookAt(point);
    }
    // 中継地点のある移動
    public void MovePoint(Vector3 ownPos, Vector3[] relay)
    {
        transform.DOPath(relay, TimeFromDistanceIncludeRelayPos(relay, ownPos), PathType.CatmullRom).SetEase(Ease.Linear).OnComplete(Stop);
    }
    // 止まったことを伝えるためのメソッド
    private void Stop()
    {
        isStoped_ = true;
        animator_.SetFloat("Speed", 0.0f);
    }
    // 現在のID取得
    public ID GetCurrentID()
    {
        return currentID_;
    }
    public void SetCurrentID(ID inID)
    {
        currentID_ = inID;
    }
    // ひとつ前のIDを取得
    public ID GetBeforeID()
    {
        return beforeID_;
    }
 
    // 中継座標を含む距離から時間を算出する
    private float TimeFromDistanceIncludeRelayPos(Vector3[] relay, Vector3 ownPos)
    {
        var distance = relay[0] - ownPos;
        for (int i = 0; i < relay.Count() - 1; i++)
        {
            distance += relay[i + 1] - relay[i];
        }
        return distance.magnitude / speed;
    }

    public void SetIdAndPosision(Vector3 pos, ID id)
    {
        this.transform.position = pos;
        currentID_ = id;
    }
    public DIR GetDirByPos(ref EventParam ep, Vector3 pos)
    {
        var id = ep.player.GetCurrentID();
        var curPos = ep.mapMng.GetMasuByID(id).Pos;
        var x = Mathf.Abs(pos.x - curPos.x); // 上方向
        var z = Mathf.Abs(pos.z - curPos.z); // 左方向

        // 性質上変化量 0 はない
        Assert.IsFalse(x == 0 && z == 0);
        if(x < z)
        {
            if(pos.z > curPos.z)
            {
                return DIR.LEFT;
            }
            else
            {
                return DIR.RIGHT;
            }
        }
        else
        {
            if(pos.x > curPos.x)
            {
                return DIR.UP;
            }
            else
            {
                return DIR.DOWN;
            }
        }
    }

    public DIR GetReverseDir(DIR currentDir)
    {
        DIR dir = new DIR();
        switch(currentDir)
        {
            case DIR.LEFT:
                dir = DIR.RIGHT;
                break;
            case DIR.RIGHT:
                dir = DIR.LEFT;
                break;
            case DIR.UP:
                dir = DIR.DOWN;
                break;
            case DIR.DOWN:
                dir = DIR.UP;
                break;
            case DIR.MAX:
                Assert.IsTrue(false);
                break;
            default:
                Assert.IsTrue(false);
                break;
        }
        return dir;
    }

    // 最初の一回のみ読む絶対
    public void SetUP(GameObject cUI, int id, PlayerSkin playerSkin)
    {
        lifeObj_ = cUI.transform.Find("Param/Life").GetComponent<Text>();
        var keyItemUI = cUI.transform.Find("Param");
        var keyItem = keyItemUI.Find("KeyItem").GetComponent<KeyItem>();
        keyObj_ = keyItem;
        playerID_ = id;
        playerSkin_ = playerSkin;
    }

    public void Damage(int d)
	{
        hp_ -= d;
        if(hp_ > 100)
		{
            hp_ = 100;
		}
        lifeObj_.text = hp_.ToString();

    }
}
