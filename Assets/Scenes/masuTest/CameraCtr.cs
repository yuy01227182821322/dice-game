using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCtr : MonoBehaviour
{
    public enum CameraType
    {
        Move,
        Select
    }
    

    [SerializeField]
    private List<Camera> cameras_ = null;
    private Camera mainCamera_;
    private GameObject player_;
    private List<Vector3> offset_ = new List<Vector3>();
    private int curIdx_;
    private Vector3 clickPos = new Vector3();
    private Vector3 xzVec { get { return new Vector3(1, 0, 1); } }
    private Vector3 halfScreenSize_;
    private Vector3 centerPos_;
    private bool isMoving_;
    public bool IsMoving => isMoving_;
    
    // Start is called before the first frame update
    void Awake()
    {
        
    }
    public void Init(Vector3 pos)
    {
        foreach (var camera in cameras_)
        {
            offset_.Add(camera.transform.position);
            camera.transform.position += pos;
        }
        curIdx_ = cameras_.FindIndex(camera => camera.name == "MoveCamera");
        mainCamera_ = cameras_[curIdx_];
        mainCamera_.gameObject.SetActive(true);

        halfScreenSize_ = new Vector3(Screen.width, Screen.height, 0) * 0.5f;
        centerPos_ = mainCamera_.ScreenToWorldPoint(halfScreenSize_);
        clickPos = centerPos_;
    }

    // Update is called once per frame
    void Update()
    {
		if((int)CameraType.Move == curIdx_)
		{
			mainCamera_.transform.position = player_.transform.position + offset_[curIdx_];
		}
		else
		{
			if(Input.GetMouseButton(0))
			{
				var beforePos = clickPos;
				clickPos = mainCamera_.ScreenToWorldPoint(Input.mousePosition);
				if(Mathf.Abs(Vector3.Distance(clickPos, beforePos)) < 2.0f)
				{
					clickPos = beforePos;
				}

			}
			centerPos_ = mainCamera_.ScreenToWorldPoint(halfScreenSize_);

			// クリックした座標と中心座標の差がないときは動かない
			if(Mathf.Abs(Vector3.Distance(clickPos, centerPos_)) < 0.1f)
			{
				isMoving_ = false;
				return;
			}
			isMoving_ = true;
			// ベクトルをxz平面にしたのち正規化
			var vec = clickPos - centerPos_;
			vec.Scale(xzVec);
			vec.Normalize();
			mainCamera_.transform.position += vec * 0.1f;
		}
	}

    // オブジェクトを追従する設定
    public void SetCamera(GameObject obj)
    {
        player_ = obj;   
    }
    public Camera GetMainCamera()
    {
        return mainCamera_;
    }

    public void ChangeCamera(CameraType cameraType)
    {
        mainCamera_.gameObject.SetActive(false);
        curIdx_ = (int)cameraType;
        mainCamera_ = cameras_[(int)cameraType];
        mainCamera_.gameObject.SetActive(true);
        InitPosition();
        centerPos_ = mainCamera_.ScreenToWorldPoint(halfScreenSize_);
        clickPos = centerPos_;
    }

    public void InitPosition()
    {
        mainCamera_.transform.position = player_.transform.position + offset_[curIdx_];
    }
}
