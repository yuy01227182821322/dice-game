using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.AddressableAssets;
using UnityEngine.Assertions;
using UnityEngine.ResourceManagement.AsyncOperations;

public class GameSceneCtr : MonoBehaviour
{
    public Transform playerParent_;
    public CameraCtr camObj;
   
    private TextBox textBox_;
   

    private EventCtr EventCtr_;
    private MapMng mapMng_;

    // ボタン位置テーブル(各方向分)
    private Vector3[] buttonTable = {new Vector3(-300, 0),  // LEFT
                                     new Vector3(300, 0),   // RIGHT
                                     new Vector3(0, 300),   // UP
                                     new Vector3(0, -300)}; // DOWN
    // キャラUI位置テーブル(各方向分)
    private Vector3[] charUITable = {new Vector3(-770, 440),  // LEFTUP
                                     new Vector3(770, 440),   // RIGHTUP 
                                     new Vector3(-770, -440),  // LEFTDOWN
                                     new Vector3(770, -440)}; // RIGHTDOWN


    // Start is called before the first fame update
    void Start()
    {
       
        mapMng_ = this.GetComponent<MapMng>();
        //// カメラ初期位置の設定(StartPosの設定)
        camObj.Init(mapMng_.GetMasuByID(mapMng_.GetStartID()).Pos);

        // これはプレイヤー順番決めのシーンからとってくるためなくなる
        int cnt = 0;
        List<Player> players = new List<Player>();
		// プレイヤーパラメータ表示UI
		var cUIHandle = Addressables.LoadAssetAsync<GameObject>("CharUI.prefab");
        cUIHandle.WaitForCompletion();
        var canvas = GameObject.FindGameObjectWithTag("Canvas").transform;
        foreach (Transform playerTransform in playerParent_)
        {
            cnt++;
            var cUI = GameObject.Instantiate(cUIHandle.Result, canvas);
            cUI.transform.localPosition = charUITable[cnt - 1];
            var player = playerTransform.gameObject.GetComponent<Player>();
            player.SetCurrentID(mapMng_.GetStartID());
            player.SetUP(cUI, 0, PlayerSkin.BLACK);
            playerTransform.name = cnt + "P";
            playerTransform.position = mapMng_.GetMasuByID(player.ID).Pos;
            players.Add(player);

            // プレイヤー設定後の設定
            var uiName = cUI.transform.Find("Name");
            uiName.Find("char").GetComponent<Text>().text = playerTransform.name;           
            cUI.name = cUI.name + playerTransform.name;
        }
        EventCtr_ = new EventCtr(ref players, ref mapMng_, ref textBox_);

        // UI作成
        var bHandle = Addressables.LoadAssetAsync<GameObject>("Assets/Scenes/masuTest/Prefab/Arrow.prefab");
        bHandle.WaitForCompletion();
        Assert.IsFalse(bHandle.Status != AsyncOperationStatus.Succeeded);
        var moveUI = canvas.Find("MoveEvent/Arrow");
        for (int i = 0; i < buttonTable.Length; i++)
        {
            var button = GameObject.Instantiate(bHandle.Result, moveUI);
            button.transform.localPosition = buttonTable[i];
            button.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        EventCtr_.Update();
    }
}
