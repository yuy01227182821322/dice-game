using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NegotiationSelectEvent : Event
{
    enum BattlePhase
    {
        Non,
        Own,
        Other,
        Robbery
    }
    BattlePhase battlePhase_;
    int ownNum_ = 0;
    int otherNum_ = 0;
    enum PeacePhase
	{
        Non,
        Own,
        Other,
        Trade
    }
    PeacePhase peacePhase_;
    ItemManager ownItem_;
    ItemManager otherItem_;

    private float time;
    private uint moveCnt_;
    private bool isSelect_;
    private IEnumerator enumerator;
    private ChooseUI chooseUI_;
    private Player otherPlayer_;
    private GameObject canvas_;
    private ItemList itemList_;
    private DiceManager diceManager_;
    public NegotiationSelectEvent(ref EventParam eventP, uint moveCnt, Player otherPlayer) : base(ref eventP)
    {
        moveCnt_ = moveCnt;
        otherPlayer_ = otherPlayer;
    }
    public override void EventStart()
    {
        isSelect_ = false;
        eventParam_.textBox.SetActive(true);
        diceManager_ = GameObject.FindGameObjectWithTag("GameController").GetComponent<DiceManager>();
        diceManager_.Init();

        var text = "他プレイヤーと接触しました\n交渉しますか？";
		eventParam_.textBox.AddText(text);
        canvas_ = GameObject.FindGameObjectWithTag("Canvas");
        chooseUI_ = canvas_.transform.Find("ChooseUI").GetComponent<ChooseUI>();
        chooseUI_.SettingAndActive(() => 
            {
                chooseUI_.gameObject.SetActive(false);
                eventParam_.textBox.Init();
                var text = "交渉を開始します\n武力と平和どちらにしますか？";
                eventParam_.textBox.AddText(text);
                chooseUI_.SetChoseObjText("武力", "平和");
                chooseUI_.SettingAndActive(BattleNegotiation, PeaceNegotiation);
            },
            () => 
            {
                chooseUI_.gameObject.SetActive(false);
                eventParam_.textBox.Init();
                var text = "交渉しませんでした";
                eventParam_.textBox.AddText(text);
                if(moveCnt_ == 0)
                {
                    eventParam_.nextEvent = new TurnEnd(ref eventParam_);
                    return;
                }
                eventParam_.nextEvent = new MoveSelectEvent(ref eventParam_, moveCnt_);
                
            });
        
    }
    public override void EventUpdate()
    {
        if(isSelect_)
        {
            time += Time.deltaTime;
            if(time > 3)
			{
                eventParam_.textBox.SetActive(false);
                eventParam_.nextEvent = new TurnEnd(ref eventParam_);
            }
            return;
        }
        // 平和交渉
        if (peacePhase_ != PeacePhase.Non)
        {
			switch(peacePhase_)
			{
				case PeacePhase.Non:
					break;
				case PeacePhase.Own:
                    if(itemList_.GetSelectItem() != null)
					{
                        ownItem_ = itemList_.GetSelectItem();
                        itemList_.Close();
                        itemList_.SetUpAndActive(otherPlayer_.Inventory.items);
                        itemList_.transform.Find("Close").gameObject.SetActive(false);
                        peacePhase_ = PeacePhase.Other;
                        eventParam_.textBox.Init();
                        var text2 = otherPlayer_.PlayerID_ + "Pの交換アイテムの選択";
                        eventParam_.textBox.AddText(text2);
                    }
					break;
				case PeacePhase.Other:
                    if(itemList_.GetSelectItem() != null)
                    {
                        otherItem_ = itemList_.GetSelectItem();
                        itemList_.Close();
                        itemList_.transform.Find("Close").gameObject.SetActive(true);
                        peacePhase_ = PeacePhase.Trade;
                    }
                    break;
				case PeacePhase.Trade:
                    otherPlayer_.Inventory.Removed(otherItem_);
                    otherPlayer_.Inventory.Add(ownItem_);
                    eventParam_.player.Inventory.Removed(ownItem_);
                    eventParam_.player.Inventory.Add(otherItem_);
                    eventParam_.textBox.Init();
                    var text = "交換終了";
                    eventParam_.textBox.AddText(text);
                    isSelect_ = true;
                    break;
				default:
					break;
			}
			return;
        }

        // 武力交渉
        if(battlePhase_ != BattlePhase.Non)
		{
			switch(battlePhase_)
			{
				case BattlePhase.Non:
					break;
				case BattlePhase.Own:
                    if(!diceManager_.IsEnd)
                    {
                        if(Input.GetMouseButtonUp(0))
                        {
                            diceManager_.Roll();
                        }
                    }
                    // ダイスアニメ終了
                    else if(diceManager_.IsEnd)
                    {
                        ownNum_ = diceManager_.GetDiceResult() + 1;
                        eventParam_.textBox.Init();
                        eventParam_.textBox.AddText(otherPlayer_.PlayerID_ + "Pの手番\nクリックでダイスを振る");
                        diceManager_.Init();
                        diceManager_.CreateDice(0);
                        battlePhase_ = BattlePhase.Other;
                    }
                    break;
				case BattlePhase.Other:
                    if(!diceManager_.IsEnd)
                    {
                        if(Input.GetMouseButtonUp(0))
                        {
                            diceManager_.Roll();
                        }
                    }
                    // ダイスアニメ終了
                    else if(diceManager_.IsEnd)
                    {
                        otherNum_ = diceManager_.GetDiceResult() + 1;
                        battlePhase_ = BattlePhase.Robbery;
                    }
                    break;
				case BattlePhase.Robbery:
                    var text = "";
                    if(ownNum_ > otherNum_)
					{
                        text = "勝利　強奪成功\nアイテムをランダムに入手\n相手に20ダメージ";
                        int rand = Random.Range(0, otherPlayer_.Inventory.GetInventoryCount());
                        var lostItem = otherPlayer_.Inventory.items[rand];
                        if(lostItem.itemID >= ItemManager.ItemID.Key_1)
                        {
                            otherPlayer_.KeyObj.SetKeyItem((int)lostItem.itemID, false);
                            eventParam_.player.KeyObj.SetKeyItem((int)lostItem.itemID, true);
                        }
                        otherPlayer_.Inventory.Removed(lostItem);
                        otherPlayer_.Damage(20);
                        eventParam_.player.Inventory.Add(lostItem);
                    }
                    else
					{
                        eventParam_.player.Damage(40);
                        text = "敗北　強奪失敗\n手痛い反撃を受け40ダメージ";
                    }
                    eventParam_.textBox.Init();
                    eventParam_.textBox.AddText(text);
                    isSelect_ = true;
                    break;
				default:
					break;
			}
		}

        //if(enumerator != null)
        //{
        //	bool result = enumerator.MoveNext();
        //	if(!result)
        //	{
        //		enumerator = null;
        //	}
        //}
        //else
        //{
        //	if(Input.GetKeyDown(KeyCode.Return))
        //	{
        //		Debug.Log("交渉を開始");
        //		// SceneManager.LoadScene("遷移テスト", LoadSceneMode.Additive);
        //		//eventParam_.nextEvent = new TurnEnd(ref eventParam_);
        //		enumerator = AddScene();
        //	}
        //}
    }
    //IEnumerator AddScene()
    //{
    //    //シーンを非同期で追加する
    //    SceneManager.LoadScene("遷移テスト", LoadSceneMode.Additive);
    //    //シーン名を指定する
    //    Scene scene = SceneManager.GetSceneByName("遷移テスト");
    //    while(!scene.isLoaded)
    //    {
    //        yield return null;
    //    }
    //    //指定したシーン名をアクティブにする
    //    SceneManager.SetActiveScene(scene);
    //    // 自分のシーンを非アクティブに
    //    GameObject.FindGameObjectWithTag("root").SetActive(false);
    //}

    private void PeaceNegotiation()
	{
        peacePhase_ = PeacePhase.Own;
        battlePhase_ = BattlePhase.Non;
        itemList_ = canvas_.transform.Find("ItemList").GetComponent<ItemList>();
        itemList_.SetUpAndActive(eventParam_.player.Inventory.items);
        itemList_.transform.Find("Close").gameObject.SetActive(false);
        chooseUI_.gameObject.SetActive(false);
        eventParam_.textBox.Init();
        var text = eventParam_.player.PlayerID_ + "Pの交換アイテムの選択";
        eventParam_.textBox.AddText(text);
    }

    private void BattleNegotiation()
    {
        peacePhase_ = PeacePhase.Non;
        battlePhase_ = BattlePhase.Own;
        diceManager_.CreateDice(0);
        eventParam_.textBox.Init();
        eventParam_.textBox.AddText(eventParam_.player.PlayerID_ + "Pの手番\nクリックでダイスを振る");
        chooseUI_.gameObject.SetActive(false);
    }
}
