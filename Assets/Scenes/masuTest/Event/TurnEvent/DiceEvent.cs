using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceEvent : Event
{
    private bool isRoll_;
    private DiceManager diceManager_;
    private ItemManager item_;
    public DiceEvent(ref EventParam eventP, ItemManager item = null) : base(ref eventP)
    {
        item_ = item;
        diceManager_ = GameObject.FindGameObjectWithTag("GameController").GetComponent<DiceManager>();
        diceManager_.Init();
    }
    public override void EventStart()
    {
        Debug.Log("ダイスイベント");
        isRoll_ = false;

        int id = 0;
        if(item_ != null)
		{
            id = ((int)item_.itemID) + 1;
		}
        // ダイス生成
        diceManager_.CreateDice(id);
        eventParam_.textBox.SetActive(true);
        eventParam_.textBox.AddText("左クリックでダイスを振る");
        eventParam_.textBox.AddText("右クリックで前に戻る");

    }
    public override void EventUpdate()
    {
        // ダイス投げる
        if(!diceManager_.IsEnd)
        {
            if(isRoll_)
			{
                return;
			}
            if(Input.GetMouseButtonUp(0))
            {
                diceManager_.Roll();
                eventParam_.player.Inventory.Removed(item_);
                isRoll_ = true;
            }
            if(Input.GetMouseButtonUp(1))
            {
                eventParam_.nextEvent = new ActionSelect(ref eventParam_);
                diceManager_.DestroyDice();
                eventParam_.textBox.SetActive(false);
                isRoll_ = true;
            }
        }
        // ダイスアニメ終了
        else if(diceManager_.IsEnd)
        {
            eventParam_.textBox.SetActive(false);
            uint moveCnt = (uint)diceManager_.GetDiceResult() + 1;
            Debug.Log("ダイス目は" + moveCnt);
            // 移動イベントへ
            eventParam_.nextEvent = new MoveSelectEvent(ref eventParam_, moveCnt);
        }
    }
}
