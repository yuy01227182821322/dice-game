using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
public class MoveSelectEvent : Event
{
    private bool moveMode_; // 選択移動かどうか
    private readonly uint moveCnt_;  // 移動回数(最初に代入後変更不可)
    private List<Player> otherPlayers_ = new List<Player>(); // 自分以外のプレイヤー
    private List<LinkedList<ID>> firstPathData_;    // 最初のパスのデータ
    private List<LinkedList<ID>> currentPathData_;        // 現在のパスデータ
    private GameObject RemainMoveUI_;
    private Text RemainMoveText_;
    //*private StepMove stepMove;
    //private SelectDestination selectDest_;

    private MoveEvent moveEvent_;
    private Button resetButton_;
    
    // コンストラクタ
    public MoveSelectEvent(ref EventParam eventP, uint moveCnt) : base(ref eventP)
    {
        moveCnt_ = moveCnt;
        // 初期は1マス移動モード
        moveMode_ = false;

        // ルート探索して結果をキャッシュしておく
        PathFinding find = new PathFinding();
        firstPathData_ = find.FindStart(ref eventP, moveCnt_);
        currentPathData_ = new List<LinkedList<ID>>(firstPathData_);
       
        // UI関連
        var moveUI = GameObject.FindGameObjectWithTag("Canvas").transform.Find("MoveEvent");
        RemainMoveUI_ = moveUI.Find("RemainMove").gameObject;     
        RemainMoveText_ = RemainMoveUI_.transform.Find("RemainNum").GetComponent<Text>();

        RemainMoveText_.text = moveCnt_.ToString();
        RemainMoveUI_.SetActive(true);

        var pParent = GameObject.FindWithTag("playerParent").transform;
        foreach (Transform playerTransform in pParent)
        {
            if(eventP.player.name == playerTransform.name)
            {
                continue;
            }
            otherPlayers_.Add(playerTransform.gameObject.GetComponent<Player>());
        }
        // モードチェンジボタン
        var modeChangeButton = moveUI.transform.Find("ModeChange").GetComponent<Button>();
        // 初期化ボタンを押したときの設定
        modeChangeButton.gameObject.SetActive(true);
        modeChangeButton.onClick.AddListener(ChangeMoveMode);
        // リセットボタン
        ResetButtonSetting(moveUI);
    }

    // イベント開始
    public override void EventStart()
    { 
        Debug.Log("移動イベント");
        if (!moveMode_)
        {
            moveEvent_ =  new StepMove(ref eventParam_, moveCnt_);
        }
        else
        {
            moveEvent_ = new SelectDestination(ref eventParam_, moveCnt_, ref currentPathData_);
        }
    }
    // イベント更新
    public override void EventUpdate()
    {
  
        moveEvent_.MoveUpdate();
        var moveCnt = moveEvent_.GetRemainMoveCnt();
        RemainMoveText_.text = moveCnt.ToString();
        if (moveEvent_.IsEnd)
        {
            eventParam_.nextEvent = moveEvent_.EventParam.nextEvent;
            MoveEnd();
        }
    }
    // 移動終了
    private void MoveEnd()
    {   
        RemainMoveUI_.SetActive(false);
        eventParam_.player.MovingDIR.Clear();
        // 止まったマスのイベント
        if(eventParam_.nextEvent as NegotiationSelectEvent == null)
        {
            eventParam_.nextEvent = new MasuEvent(ref eventParam_);
        }
    }


    private void ChangeMoveMode()
    {
        moveEvent_.ModeChangeFunc();
        moveMode_ = !moveMode_;
        if(moveMode_)
        {
            PathFinding find = new PathFinding();
            firstPathData_ = find.FindStart(ref eventParam_, moveEvent_.GetRemainMoveCnt());
            currentPathData_ = new List<LinkedList<ID>>(firstPathData_);
            moveEvent_ = new SelectDestination(ref eventParam_, moveCnt_, ref currentPathData_);
        }
        else
        {
            moveEvent_ = new StepMove(ref eventParam_, moveCnt_);
        }
    }
    private void ResetButtonSetting(Transform moveUI)
    {
        resetButton_ = moveUI.transform.Find("Reset").GetComponent<Button>();
        // 初期化ボタンを押したときの設定
        resetButton_.gameObject.SetActive(true);

        resetButton_.onClick.AddListener(() =>
        {
            moveEvent_.ResetFunc();
            if (moveEvent_ as SelectDestination != null)
            {
                currentPathData_ = new List<LinkedList<ID>>(firstPathData_);
                (moveEvent_ as SelectDestination).ReCreateSelectMasu();
            }
        });
    }
}
