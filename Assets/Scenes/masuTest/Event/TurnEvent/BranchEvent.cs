using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BranchEvent : Event
{
    private bool isSelect;
    public BranchEvent(ref EventParam eventP) : base(ref eventP)
    {

    }
    public override void EventStart()
    {
        Debug.Log("分岐イベント");
        
        isSelect = false;
    }
    public override void EventUpdate()
    {
        if(!isSelect)
        {
            // 選択
            // 分岐するかどうか今は分岐確定
            //var masu = eventParam_.mapMng.GetMassByID(eventParam_.player.GetMasuAndRootID());
            isSelect = true;
            return;
        }
        // 選択後
        uint moveCnt = 1;
        eventParam_.nextEvent = new MoveSelectEvent(ref eventParam_, moveCnt);
    }
}
