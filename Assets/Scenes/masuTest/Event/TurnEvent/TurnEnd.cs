using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnEnd : Event
{
    private bool isSelect_;
    public TurnEnd(ref EventParam eventP) : base(ref eventP)
    {
       
    }
    public override void EventStart()
    {
        Debug.Log("ターン終了");
        isFinished_ = true;
    }
    public override void EventUpdate()
    {
        // 最初へ　
        eventParam_.nextEvent = new ActionSelect(ref eventParam_);
    }
}
