using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActionSelect : Event
{
    private bool isSelect_;
    private bool itemSelect_;
    private GameObject canvas_;
    private Transform selectUI_;
    private Button diceRollButton_;
    private Button aroundViewButton_;
    private Button aroundViewEndButton_;
    private Button useItemButton_;
    private Event nextEvent_;
    private ItemList itemList_;
    private CameraCtr cameraCtr_;
    public ActionSelect(ref EventParam eventP) : base(ref eventP)
    {
    }
    public override void EventStart()
    {
        Debug.Log(eventParam_.player.name);
        Debug.Log("開始and選択イベント");
        cameraCtr_ = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraCtr>();
        Debug.Log(cameraCtr_);
        cameraCtr_.SetCamera(eventParam_.player.gameObject);
        canvas_ = GameObject.FindGameObjectWithTag("Canvas");
        var curPlayerUI = canvas_.transform.Find("CurrentPlayer");
        curPlayerUI.Find("CurrentText").GetComponent<Text>().text = eventParam_.player.PlayerID_ + "P";
        selectUI_ = canvas_.transform.Find("SelectUI");
        selectUI_.gameObject.SetActive(true);
        diceRollButton_ = selectUI_.Find("DiceRoll").GetComponent<Button>();
        diceRollButton_.onClick.AddListener(OnDiceRollButton);
        aroundViewButton_ = selectUI_.Find("ViewAround").GetComponent<Button>();
        aroundViewButton_.onClick.AddListener(OnAroundViewButton);
        aroundViewEndButton_ = selectUI_.Find("ViewEnd").GetComponent<Button>();
        aroundViewEndButton_.onClick.AddListener(OnViewEndButton);
        aroundViewEndButton_.gameObject.SetActive(false);
        useItemButton_ = selectUI_.Find("UseItem").GetComponent<Button>();
        useItemButton_.onClick.AddListener(OnUseItemButton);
        itemSelect_ = false;
    }
    public override void EventUpdate()
    {
        if(itemSelect_)
		{
            var item = itemList_.GetSelectItem();
            if(item != null)
			{
                //アイテム使用
                nextEvent_ = new DiceEvent(ref eventParam_, item);
                itemList_.Close();
                Select();
            }
        }
        if (!isSelect_)
        {           
            return;
        }
        // 選択後
        eventParam_.nextEvent = nextEvent_;
    }

    void OnDiceRollButton()
	{
        nextEvent_ = new DiceEvent(ref eventParam_);
        Select();
    }
    void OnAroundViewButton()
	{
        cameraCtr_.ChangeCamera(CameraCtr.CameraType.Select);
        aroundViewButton_.gameObject.SetActive(false);
        useItemButton_.gameObject.SetActive(false);
        diceRollButton_.gameObject.SetActive(false);
        aroundViewEndButton_.gameObject.SetActive(true);
    }

    void OnViewEndButton()
    {
        cameraCtr_.ChangeCamera(CameraCtr.CameraType.Move);
        aroundViewButton_.gameObject.SetActive(true);
        useItemButton_.gameObject.SetActive(true);
        diceRollButton_.gameObject.SetActive(true);
        aroundViewEndButton_.gameObject.SetActive(false);
    }

    void OnUseItemButton()
    {
        //nextEvent_ = new DiceEvent(ref eventParam_);
        itemList_ = canvas_.transform.Find("ItemList").GetComponent<ItemList>();
        itemList_.SetUpAndActive(eventParam_.player.Inventory.items);
        itemList_.CloseButtonAddListener(() => { selectUI_.gameObject.SetActive(true); });
        selectUI_.gameObject.SetActive(false);
        itemSelect_ = true;
    }
    void Select()
	{
        selectUI_.gameObject.SetActive(false);
        aroundViewButton_.onClick.RemoveAllListeners();
        useItemButton_.onClick.RemoveAllListeners();
        diceRollButton_.onClick.RemoveAllListeners();
        aroundViewEndButton_.onClick.RemoveAllListeners();
        // 選択終了
        isSelect_ = true;

    }
}
