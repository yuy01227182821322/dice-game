using UnityEngine;
using System;
public class MasuEvent : Event
{
    private string currentText_;
    private Masu masu_;
    public MasuEvent(ref EventParam eventP) : base(ref eventP)
    {
        // イベント取得
        masu_ = eventP.mapMng.GetMasuByID(eventP.player.GetCurrentID());
        Debug.Log(masu_.Type.ToString());
        

    }

    // イベント開始
    public override void EventStart()
    {
        masu_.EventStart(eventParam_);
        eventParam_.textBox.SetActive(true);
        currentText_ = masu_.GetText();
        eventParam_.textBox.AddText(currentText_);
    }
    // イベント更新
    public override void EventUpdate()
    {
        if(masu_.IsEnd)
		{
            eventParam_.textBox.SetActive(false);
            eventParam_.nextEvent = new TurnEnd(ref eventParam_);
        }
        masu_.EventUpDate(ref eventParam_);
        if(masu_.Type == MasuType.DAMAGE)
        {
            if(currentText_ != masu_.GetText())
            {
                eventParam_.textBox.AddText(masu_.GetText());
                currentText_ += masu_.GetText();
            }
        }
    }
}
