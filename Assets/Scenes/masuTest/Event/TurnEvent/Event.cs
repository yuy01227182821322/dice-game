public struct EventParam
{
    public Player player;   // プレイヤー
    public MapMng mapMng;   // mapMng
    public Event nextEvent; // 次のイベント
    public TextBox textBox; // テキストボックス
    public EventParam(Player p, ref MapMng m, Event ne, ref TextBox tb)
    {
        player = p;
        mapMng = m;
        nextEvent = ne;
        textBox = tb;
    }
}
public abstract class Event
{
    protected EventParam eventParam_;
    public EventParam EventParam => eventParam_; 
    protected bool isFinished_ = false;  // 終了していたら

    public virtual bool IsFinished => isFinished_;
    // 次のイベントを取得設定
   // public virtual Event NextEvent{ get { return nextEvent_; } set { nextEvent_ = value; } }
    public Event(ref EventParam eventParam)
    {
        eventParam_ = eventParam;
    }

    // イベント開始
    public abstract void EventStart();
    // イベント更新
    public abstract void EventUpdate();
   
    public virtual void ChangePlayer(Player player)
    {
        eventParam_.player = player;
    }
}
