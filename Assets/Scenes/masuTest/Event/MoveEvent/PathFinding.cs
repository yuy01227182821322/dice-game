using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System.Linq;


// ブランチマスまでの経路探索

public class PathFinding
{
    // 前に進むルートと後ろに進むルートの二つのみ
    List<LinkedList<ID>> resultPathList_ = new List<LinkedList<ID>>();
    private const uint branchCnt_ = 2;
    private ID currentID_;
    public List<LinkedList<ID>> FindStart(ref EventParam eventParam, uint moveCnt)
    {
        List<LinkedList<ID>> pathList = new List<LinkedList<ID>>();

        // 最初に自分のマスとその次のマスの設定
        currentID_ = eventParam.player.GetCurrentID();
        var nextID =  eventParam.mapMng.NextIDList(currentID_);
        uint moveC = moveCnt;
        foreach (var nID in nextID)
        {
            // マスのIDが0なら侵入不可なので除外
            if (nID.masuID == 0)
            {
                continue;
            }
            pathList.Add(new LinkedList<ID>());
            pathList.Last().AddLast(nID);
            // 次のIDが0であるなら設定
            if (nID.rootID == 0)
            {
                resultPathList_.Add(pathList.Last());
            }

        }
        //1マス先を先に設定しているためmoveCntを1つ減らす
        moveC--;
        // moveCntが0なら移動終了のため
        if(moveC == 0)
        {
            foreach (var path in pathList)
            {
                resultPathList_.Add(path);
            }
        }
        // 再帰的にルート作成
        if (resultPathList_.Count < pathList.Count || moveC > 0)
        {
            RecursiveRootMove(ref eventParam, ref pathList, moveC);
        }
        return resultPathList_;
    }
    private void RecursiveRootMove(ref EventParam eventParam, ref List<LinkedList<ID>> RecursivePassList,uint moveCnt)
    {
        List<List<ID>> idList = new List<List<ID>>();
        List<ID> nextIDs = new List<ID>();
        // moveCntの設定
        moveCnt--;
        // パスをまとめたリストからパスをひとつずつ取り出す
        foreach (var path in RecursivePassList)　// 最大分岐分まで
        {
            // rootIDが0ならたどり着いているためコンテニュー
            if (path.Last.Value.rootID == 0)
            {
                continue;
            }
            nextIDs = eventParam.mapMng.NextIDList(path.Last.Value);
            // 移動前がパスに含まれていたら次の移動先リストから削除
            nextIDs.RemoveAll(id => path.Contains(id)|| currentID_ == id);
            // nextIDs[0]を調べているパスにAddする
            // 正しい結果ではnextIDは1つになるはず
            // 袋小路に行ったときはカウントが0になるため設定
            if(nextIDs.Count != 1)
            {
                resultPathList_.Add(path);
                continue;
            }
            path.AddLast(nextIDs[0]);
            // 分岐マスに到達したか移動カウンタが0の場合
            // 到達済みなので結果をリストに登録したい
            if (path.Last.Value.rootID == 0 || moveCnt == 0)
            {
                if (CheckEndID(path))
                {
                    resultPathList_.Add(path);
                }
            }
        }
      
        // 移動終了でなかったら再帰
        // 全部到達していたら終了
        // 終了条件は進む方向の数分結果が出ていたら
        // または、移動カウンタ分出ていたら
        if(resultPathList_.Count < RecursivePassList.Count || moveCnt > 0)
        {
            RecursiveRootMove(ref eventParam,ref RecursivePassList, moveCnt);
        }
        return;
    }

    private bool CheckEndID(LinkedList<ID> path)
    {
        // パスの到達点が被っている場合短いほうを採用するため
        // 後から登録するものははじく
        foreach (var resultPath in resultPathList_)
        {
            if (path.Last == resultPath.Last)
            {
                return false;
            }
        }
        return true;
    }

}
