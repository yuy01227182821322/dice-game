using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;

// マス移動イベント
public abstract class MoveEvent
{
    public struct ResetParam
    {
    }
    protected uint moveCnt_;
    protected ForcedMove forcedMove_;
    protected CameraCtr cameraCtr_;

    protected EventParam eventParam_;

    public EventParam EventParam => eventParam_;
    // 移動初めのマスのID
    protected ID resetID_;

    protected Button resetButton_;

    protected bool isEnd_;
    public bool IsEnd => isEnd_;

    protected Button modeChangeButton_;


    protected MoveEvent(ref EventParam ep, uint moveCnt)
    {
        cameraCtr_ = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraCtr>();
        moveCnt_ = moveCnt;
        eventParam_ = ep;
        resetID_ = ep.player.ID;
        var moveUI = GameObject.FindGameObjectWithTag("Canvas").transform.Find("MoveEvent");
        resetButton_ = moveUI.Find("Reset").GetComponent<Button>();
        modeChangeButton_ = moveUI.Find("ModeChange").GetComponent<Button>();
    }

    abstract public void MoveStart();
    abstract public void MoveUpdate();
    abstract public void MoveEnd();

    public uint GetRemainMoveCnt()
    {
        return moveCnt_ - (uint)eventParam_.player.MovingDIR.Count;
    }

    public virtual void ResetFunc()
    {
        eventParam_.player.SetIdAndPosision(eventParam_.mapMng.GetMasuByID(resetID_).Pos, resetID_);
        eventParam_.player.MovingDIR.Clear();
    }
    public virtual void ModeChangeFunc()
    {
        cameraCtr_.InitPosition();
    }

}
