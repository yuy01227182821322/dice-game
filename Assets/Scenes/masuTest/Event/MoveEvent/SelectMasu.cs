using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectMasu : MonoBehaviour
{
    private LinkedList<ID> path;
    public LinkedList<ID> Path => path;
    private GameObject obj;
    public GameObject Obj => obj;
    public void SetUp(LinkedList<ID> p, GameObject o)
    {
        path = p;
        obj = o;
    }   
}
