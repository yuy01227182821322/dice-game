using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

// 1マス移動するclass
public class ForcedMove
{
    private uint moveCnt_;        // 移動回数
    public uint MoveCnt => moveCnt_;
    private List<Player> otherPlayers_ = new List<Player>(); // 自分以外のプレイヤー
    private EventParam eventParam_;
    private LinkedList<ID> specifyPathList_;  // 指定パス
    private bool back_ = false;
    private bool isEnd_;
    public bool IsEnd => isEnd_;
    private ID nextID_;

    // パス指定して動かすことができるコンストラクタ
    public ForcedMove(ref EventParam eventP,LinkedList<ID> specifyPath) 
    {
        specifyPathList_ = specifyPath;
        CommonInit(ref eventP);
    }
    // 移動カウンタのみのコンストラクタ
    public ForcedMove(ref EventParam eventP, uint moveCnt, bool back  = false)
    {
        back_ = back;
        moveCnt_ = moveCnt;
        CommonInit(ref eventP);
    }
    // 共通部分の初期化
    private void CommonInit(ref EventParam eventP)
    {
        eventParam_ = eventP;
        var pParent = GameObject.FindWithTag("playerParent").transform;
        foreach (Transform playerTransform in pParent)
        {
            if (eventP.player.name == playerTransform.name)
            {
                continue;
            }
            otherPlayers_.Add(playerTransform.gameObject.GetComponent<Player>());
        }
    }
    // 1マス移動開始
    public void MoveStart()
    {
        isEnd_ = false;
        Vector3 nextMasuPos = new Vector3();
        var nextID = new ID();
        // パスが指定されているか？
        if (specifyPathList_ == null)
        {
            // 指定なし強制移動
            var id = eventParam_.player.GetCurrentID();
            var nextIDList = eventParam_.mapMng.NextIDList(id);
            if(!back_)
            {
                // 前動いた方と逆方向に移動
                nextIDList.RemoveAll(nId => nId == eventParam_.player.GetBeforeID() || nId == id);
                //Assert.IsTrue(nextIDList.Count == 1);
            }
			else
			{
                // 前動いた方に移動
                nextIDList.RemoveAll(nId => nId != eventParam_.player.GetBeforeID() || nId == id);
                back_ = false;  // 一度動いたら解除しないと行ったり来たりする
                //Assert.IsTrue(nextIDList.Count == 1);
            }
            // とりあえず分岐に差し掛かっても適当に移動
            nextID = nextIDList[0];
            nextMasuPos = eventParam_.mapMng.GetMasuByID(nextID).Pos;

        }
        else
        {
            // 指定あり強制移動
            nextID = specifyPathList_.First.Value;
            nextMasuPos = eventParam_.mapMng.GetMasuByID(nextID).Pos;
            specifyPathList_.RemoveFirst();
        }
        eventParam_.player.MoveMasu(nextMasuPos, nextID);
    }
    // 次のIDを指定して移動開始
    public void MoveStart(ID nextID)
    {
        var nextMasuPos = eventParam_.mapMng.GetMasuByID(nextID).Pos;

        eventParam_.player.MoveMasu(nextMasuPos, nextID);
      
    }
    // 移動中いつ終了に飛ぶか？
    // restartFlagは終了後繰り返すかどうかのフラグ
    // ID指定移動の時はfalseにしておくそれ以外の場合trueのままでよい
    public void MoveUpdate(ref EventParam eventP, bool restartFlag = true)
    {
        if (eventP.player.IsStoped && !isEnd_)
        {
            eventP.player.IsStoped = false;
            MoveEnd(ref eventP, restartFlag);
        }
    }

    // 1マス移動終了後の処理
    // restartFlagは終了後繰り返すかどうかのフラグ
    public void MoveEnd(ref EventParam eventP, bool restartFlag = true)
    {
        // 移動前の座標で方向リストに追加
        AddDIRList(ref eventP);
        // 移動カウンタを減らす
        moveCnt_--;
        // 強制系のイベント
        // ここでマスの強制イベントを調べる
        foreach (Player player in otherPlayers_)
        {
            if (player.ID == eventP.player.ID && player.Inventory.GetInventoryCount() > 0 && eventP.player.Inventory.GetInventoryCount() > 0)
            {
                eventP.nextEvent = new NegotiationSelectEvent(ref eventP, moveCnt_, player);
                isEnd_ = true;
                return;
            }
        }
        // 終了マスだったら
        if(eventP.mapMng.GetMasuByID(eventP.player.ID).Type == MasuType.END)
        {
            eventP.nextEvent = new MasuEvent(ref eventP);
            isEnd_ = true;
            return;
        }

        // パス指定移動の場合
        if(specifyPathList_ != null)
        {
            if(specifyPathList_.Count != 0)
            {
                MoveStart();
                return;
            }                 
            isEnd_ = true;
            return;
        }
        if (restartFlag)
        {
            // 移動カウンタが0でないか    
            if (moveCnt_ != 0)
            {
                // 移動カウンタが0でなかったら
                // 再度移動
                MoveStart();
                return;
            }
        }
        // ここまで抜けてきたら終了
        isEnd_ = true;
    }

    // リスタートの際呼ぶ呼ばないと終了が毎フレーム走る
    public void RestartInit()
    {
        isEnd_ = false;
    }
    // 移動方向を方向リストに追加
    // 前回のマスの座標から
    void AddDIRList(ref EventParam eventP)
    {
        // ルートに追加
        var beforeID = eventP.player.GetBeforeID();
        Player.DIR idx = eventP.player.GetDirByPos(ref eventP, eventP.mapMng.GetMasuByID(beforeID).Pos);
        idx = eventP.player.GetReverseDir(idx);
        // ルートがないとき
        if (eventP.player.MovingDIR.Count == 0)
        {
            Debug.Log("進んだ");
            eventP.player.MovingDIR.AddLast(idx);
        }
        // ルートがあるとき
        else if (eventP.player.MovingDIR.Last.Value == eventP.player.GetReverseDir(idx))
        {
            Debug.Log("戻った");
            eventP.player.MovingDIR.RemoveLast();
        }
        else
        {
            Debug.Log("進んだ");
            eventP.player.MovingDIR.AddLast(idx);
        }
    }
}
