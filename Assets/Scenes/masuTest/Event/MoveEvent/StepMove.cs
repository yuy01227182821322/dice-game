using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;


// 1マスずつ移動するクラス
// 分かれ道でも使用する
public class StepMove : MoveEvent
{
    private List<Button> arrowButton_ = new List<Button>();
   
    public StepMove(ref EventParam eventParam, uint moveCnt) : base(ref eventParam, moveCnt)
    {
        // カメラ切り替え
        cameraCtr_.ChangeCamera(CameraCtr.CameraType.Move);
        var moveUI = GameObject.FindGameObjectWithTag("Canvas").transform.Find("MoveEvent");
        var arrow = moveUI.transform.Find("Arrow");
        foreach (Transform child in arrow)
        {
            arrowButton_.Add(child.gameObject.GetComponent<Button>());
        }
        forcedMove_ = new ForcedMove(ref eventParam_, moveCnt_);

        MoveStart();
    }
    override public void MoveStart()
    {
        var currentId = eventParam_.player.GetCurrentID();
        var moveID = eventParam_.mapMng.NextIDList(currentId);
        resetButton_.gameObject.SetActive(true);
        modeChangeButton_.gameObject.SetActive(true);
        foreach (var id in moveID)
        {
            // マスのIDが0の場合侵入不可
            if (id.masuID == 0)
            {
                continue;
            }
            MoveButtonSetting(currentId, id);
        }
    }
    override public void MoveUpdate()
    {
        forcedMove_.MoveUpdate(ref eventParam_, false);
        if (forcedMove_.IsEnd && !isEnd_)
        {
            eventParam_.player.IsStoped = false;
            MoveEnd();
        }  
    }
    override public void MoveEnd()
    {
        if(eventParam_.nextEvent as NegotiationSelectEvent != null
           || eventParam_.nextEvent as MasuEvent != null)
		{
            isEnd_ = true;
            resetButton_.onClick.RemoveAllListeners();
            modeChangeButton_.onClick.RemoveAllListeners();
            return;
        }

        if(GetRemainMoveCnt() != 0)
        {
            forcedMove_.RestartInit();
            MoveStart();
            return;
        }
        isEnd_ = true;
        resetButton_.onClick.RemoveAllListeners();
        modeChangeButton_.onClick.RemoveAllListeners();
    }
    
    private void MoveButtonSetting(ID currentId, ID id)
    {
        Vector3 nextPos = eventParam_.mapMng.GetMasuByID(id).Pos;
        int idx = (int)eventParam_.player.GetDirByPos(ref eventParam_, nextPos);
        arrowButton_[idx].gameObject.SetActive(true);
        // 移動ボタンに押したときのイベントを設定
        arrowButton_[idx].GetComponent<Button>().onClick.AddListener(() =>
        {
            ClickMove(id);
            Debug.Log("rootID" + id.rootID);
            Debug.Log("masuID" + id.masuID);
            resetButton_.gameObject.SetActive(false);
            modeChangeButton_.gameObject.SetActive(false);
           
            InitArrowButton();     
        });        
    }

    private void InitArrowButton()
    {
        foreach (var button in arrowButton_)
        {
            button.gameObject.SetActive(false);
            button.onClick.RemoveAllListeners();
        }
    }

    public void ClickMove(ID id)
    {
        forcedMove_.MoveStart(id);
    }

    public override void ResetFunc()
    {
        base.ResetFunc();
        InitArrowButton();
        MoveStart();
    }

    public override void ModeChangeFunc()
    {
        base.ModeChangeFunc();
        InitArrowButton();
    }
   

}
