using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

// 分岐までの自動移動選択
public class SelectDestination : MoveEvent
{
 
    private LinkedList<SelectMasu> selMasuObj_ = new LinkedList<SelectMasu>();
    private List<LinkedList<ID>> pathList_;
    private AsyncOperationHandle<GameObject> selMasuHandle_;
   

    public SelectDestination(ref EventParam eventParam, uint moveCnt, ref List<LinkedList<ID>> pathList) : base(ref eventParam, moveCnt)
    {
        
        // カメラ切り替え
        cameraCtr_.ChangeCamera(CameraCtr.CameraType.Select);
        pathList_ = pathList;
        selMasuHandle_ = Addressables.LoadAssetAsync<GameObject>("Assets/Scenes/masuTest/Prefab/SelectMasu.prefab");
        selMasuHandle_.WaitForCompletion();
        modeChangeButton_.gameObject.SetActive(true);
        MoveStart();
    }


    override public void MoveStart()
    {
        modeChangeButton_.gameObject.SetActive(true);
        resetButton_.gameObject.SetActive(true);
        isEnd_ = false;
        CreateSelectMasu();
    }

    public void Restart(ref List<LinkedList<ID>> pathList)
    {
        cameraCtr_.ChangeCamera(CameraCtr.CameraType.Select);
        pathList_ = pathList;   
        MoveStart();
    }

    override public void MoveUpdate()
    {
        // 強制移動をしているか
        if (forcedMove_ != null)
        {
            forcedMove_.MoveUpdate(ref eventParam_);
            // 強制移動終了時
            if (forcedMove_.IsEnd)
            {
                MoveEnd();
            }
            return;
        }
        // カメラが動いているときはクリックできない
        if (cameraCtr_.IsMoving)
        {
            return;
        }
        if (Input.GetMouseButtonDown(0))
        {
            var mainCamera = cameraCtr_.GetMainCamera();
            RaycastHit hit;
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            int layerMask = LayerMask.GetMask("SelectMasu");
            if (!Physics.Raycast(ray, out hit, 100.0f, layerMask))
            {
                return;
            }
            var selMasu = hit.transform.GetComponent<SelectMasu>();

            // 移動させる
            forcedMove_ = new ForcedMove(ref eventParam_, selMasu.Path);
            forcedMove_.MoveStart();
            modeChangeButton_.gameObject.SetActive(false);
            resetButton_.gameObject.SetActive(false);
            cameraCtr_.ChangeCamera(CameraCtr.CameraType.Move);
        }
    }
    override public void MoveEnd()
    {
        foreach (var selMasu in selMasuObj_)
        {
            GameObject.Destroy(selMasu.Obj);
        }
        if(eventParam_.nextEvent as NegotiationSelectEvent == null)
        {
            // 移動カウンタが0でなかったら
            var remCnt = GetRemainMoveCnt();
            if(remCnt != 0)
            {
                // 再計算して移動
                forcedMove_ = null;
                var findPathData = new PathFinding().FindStart(ref eventParam_, remCnt);
                Restart(ref findPathData);
                return;
            }
        }

        isEnd_ = true;
       
        resetButton_.onClick.RemoveAllListeners();
        modeChangeButton_.onClick.RemoveAllListeners();
    }

    public override void ResetFunc()
    {
        base.ResetFunc();
        forcedMove_ = null;
    }
    public override void ModeChangeFunc()
    {
        base.ModeChangeFunc();
        DestroySelectMasu();
        forcedMove_ = null;
    }

    // オブジェクトを作り直す
    public void ReCreateSelectMasu()
    {
        DestroySelectMasu();
        CreateSelectMasu();
    }

    private void DestroySelectMasu()
    {
        foreach (var selMasu in selMasuObj_)
        {
            GameObject.Destroy(selMasu.Obj);
        }
    }

    private void CreateSelectMasu()
    {
        // 選択用オブジェクト生成
        foreach (var path in pathList_)
        {
            var obj = GameObject.Instantiate(selMasuHandle_.Result);
            obj.transform.position = eventParam_.mapMng.GetMasuByID(path.Last.Value).Pos;
            obj.transform.position += new Vector3(0, 2, 0);
            var selMasu = obj.GetComponent<SelectMasu>();
            selMasu.SetUp(path, obj);
            selMasuObj_.AddLast(selMasu);
        }
    }
}
