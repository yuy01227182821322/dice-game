using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.Events;


// 削除予定

//public struct MasuEventFunc
//{
//    public delegate Event EventDelegate(EventParam eventParam);
//    public UnityAction start;    // 1回のみ
//    public EventDelegate update;   // 更新毎に
//    public uint moveCnt; // 移動するイベントなら移動量
//}


//public class MasuFuncList
//{
//    private MasuType masuType_;
//    private Dictionary<MasuType, MasuEventFunc> eventList_ = new Dictionary<MasuType, MasuEventFunc>();
    
//    public void SetUp()
//    {
//        foreach(MasuType type in Enum.GetValues(typeof(MasuType)))
//        {
//            var eventFunc = new MasuEventFunc();
//            eventFunc.update = StopCallbackMethod;
//            eventFunc.moveCnt = 0;
//            eventFunc.start = () =>
//            {
//                return;
//            };
//            switch (type)
//            {
//                // バグ
//                case MasuType.NULL:
//                    Debug.Assert(true);
//                    break;
//                // 何も効果ないマス
//                case MasuType.START:
//                    eventFunc.start = () =>
//                    {
//                        Debug.Log("はじめ");
//                    };
//                    break;
//                case MasuType.BRANCH:
//                    eventFunc.start = () =>
//                    {
//                        Debug.Log("分岐はじめ");
//                    };
//                    //eventFunc.update = BranchCallbackMethod;
//                    break;
//                // ノーマル(1マス進む無限)
//                case MasuType.NORMAL:
//                    eventFunc.start = () =>
//                    {
//                        Debug.Log("何もない");
//                    };
//                    //eventFunc.moveCnt = 1;
//                    //eventFunc.update = MoveCallbackMethod;
//                    break;
//                // クリア判定
//                case MasuType.END:
//                    eventFunc.start = () =>
//                    {
//                        Debug.Log("クリア判定");
//                    };                  
//                    break;
//                // ダメージ(１マス進む)
//                case MasuType.DAMAGE:
//                    eventFunc.start = () =>
//                    {
//                        Debug.Log("ダメージを受けた");
//                    };                   
//                    break;
//                // 回復
//                case MasuType.RECOVERY:
//                    eventFunc.start = () =>
//                    {
//                        Debug.Log("回復した");
//                    };                    
//                    break;
//                // バグ
//                default:
//                   // Debug.Assert(true);
//                    break;
//            }
//            eventList_.Add(type, eventFunc);
//        }
//    }
//    // イベントを取得する
//    // その際取得したイベントのタイプを保存する
//    public MasuEventFunc GetEventFuncByType(MasuType type)
//    {
//        masuType_ = type;
//        return eventList_[type];
//    }


//    public Event MoveCallbackMethod(EventParam eventParam)
//    {
//        eventParam.moveCnt = GetEventFuncByType(masuType_).moveCnt;
//        return  new MoveSelectEvent(eventParam);
//    }

//    public Event BranchCallbackMethod(EventParam eventParam)
//    {
//        return new BranchEvent(eventParam);
//    }

//    public Event StopCallbackMethod(EventParam eventParam)
//    {
//        return new TurnEnd(eventParam);
//    }
//}
